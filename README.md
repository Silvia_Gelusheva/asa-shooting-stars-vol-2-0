<!-- <img align="left" src='./public/ASA SS.png' alt="logo" width='70px'>    <p>&nbsp;</p> -->


#  ASA Shooting Stars


### Description

Upgraded version of our final project assignment for Alpha JavaScript 42 at Telerik Academy.

A Single Page Application for creating online photo contests, aiming to inspire others and a place to share beauty of the world.

Each photo contest has three parts:
- ***open*** - users can enroll in a contest and upload a photo;
- ***review*** - time for jury to rate every participation; 
- ***finished*** - photos and winners can be seen by all users. 

The application has two main parts.
For organizers, where, the they can organize photo contests and rate them and for photo junkies – everyone who likes photography can register and participate in different contests either after invitation or enrolling themselves. Junkies with certain ranking can be invited to be a part of the jury.
<br>

### Project information

- JavaScript
- React JS
- CSS
- Daisy UI
- Tailwind UI
- Tailwind CSS
- React Hook Form
- Firebase
- ESLint
<br>

### Setup

Open the root directory photo-contest-app folder. Install all packages by running:
`npm install`

Then run:

`npm run dev`

<br>

You can test the application and see for yourself what every user can do by login to the application as a Junkie or Organiser, using the following credentials:

*email:* junkie_test@test.js
*password:* 123456

*email:* organiser_test@test.js
*password:* 123456

You can register a new user if you prefer.

*Other developers:* 
*Angelina Terzieva*,
*Anatoly Valchev*

<br>

#### **Landing page**

<img src='assets/LandingPage.png' width='600px'>

<br/>

#### **Register**

<img src='assets/Register.png' width='600px'> 

<br/>

#### **Login**

<img src='assets/Login.png' width='600px'> 

<br/>

#### **Dashboard**

<img src='assets/Dashboard.png' width='600px'> 

<br/>


#### **Create Contest**

<img src='assets/CreateContest.png' width='600px'> 

<br/>


#### **Inspire**

<img src='assets/Inspire.png' width='600px'> 

<br/>

#### **Contests**

<img src='assets/ContestList.png' width='600px'>

<br/> 

#### **Enroll**

<img src='assets/Enroll.png' width='600px'> 

<br/>

#### **Participation form**

<img src='assets/Participate.png' width='600px'> 

<br/>

#### **Reviews**

<img src='assets/Reviewed.png' width='600px'>
<br/> 

#### **All Stars**

<img src='assets/Members.png' width='600px'> 

<br/>

#### **Profile**

<img src='assets/Profile.png' width='600px'> 

<br/>



