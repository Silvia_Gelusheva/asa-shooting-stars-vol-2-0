import { AppContext } from '../../context/app.context'
import { ContestInfo } from '../../components/ContestInfo/ContestInfo'
import CreateReview from '../../components/Review/CreateReview'
import ParticipationInfo from '../../components/ParticipationInfo/ParticipationInfo'
import ReviewContent from '../../components/Review/ReviewContent'
import { getAllParticipationReviewsById } from '../../services/review.services'
import { getContestById } from '../../services/contest.services'
import { getParticipationById } from '../../services/participation.services'
import { useContext } from 'react'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useState } from 'react'

export default function SingleParticipationView() {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)
  const { contestId, phase, participationId } = useParams()
  const [allReviews, setAllReviews] = useState([])
  const [participation, setParticipation] = useState({})
  const [isJury, setIsJury] = useState(false)
  const [contest, setContest] = useState({})
  //const [reviewList, setReviewList] = useState({})
  const [myReview, setMyReview] = useState('')

  useEffect(() => {
    getContestById(contestId)
      .then(setContest)
      .catch((error) => {
        console.log(error.message)
      })
    if (contest?.juryList?.[userData?.username] || userData?.role == 2) {
      setIsJury(true)
      setMyReview(participation?.reviews?.[userData?.username])
    }
  }, [userData, allReviews])

  useEffect(() => {
    getParticipationById(participationId)
      .then(setParticipation)
      .catch((error) => console.log(error.message))
  }, [userData])

  useEffect(() => {
    getAllParticipationReviewsById(participationId)
      .then(setAllReviews)
      .catch((e) => console.log(e.message))
  }, [participationId])

  return (
    <div className='min-h-full'>
      
      <ContestInfo phase={phase} contest={contest} />
      <div className="flex flex-col lg:flex-row md:flex-row w-screen px-[5px]">
        <div className="flex-1 max-h-[600px] flex justify-center items-center p-5">
          <img src={participation?.photo} className="h-[100%] " alt="participationPhoto" />
        </div>
        <div className="flex-1 flex-col justify-center items-center">
          <ParticipationInfo
            author={participation?.author}
            title={participation?.title}
            score={participation?.score}
            description={participation?.story}
            createdOn={`${new Date(participation?.addedOn).toLocaleDateString('en-GB')}`}
            avatar={participation?.avatar}
          />
          <div>
            {isJury === true &&
              phase === 'review' &&
              (myReview !== undefined ? (
                <ReviewContent review={myReview} isActive={false} />
              ) : (
                <CreateReview setReview={setMyReview} key={'createReviewKey'} />
              ))}
            {phase === 'finished' &&
              allReviews.map((review, index) => (
                <ReviewContent key={index} review={review} isActive={false} />
              ))}
          </div>
        </div>
      </div>
      <div className="w-full flex flex-col justify-center"></div>
    </div>
  )
}
