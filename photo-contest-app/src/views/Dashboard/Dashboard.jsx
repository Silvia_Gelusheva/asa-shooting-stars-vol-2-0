import './Dashboard.css'

import { AppContext } from '../../context/app.context'
import { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

export const Dashboard = () => {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)
  const navigate = useNavigate()
  return (
    <div className="wrapper min-h-full">
      <div onClick={() => navigate('registration')} className="item">
        <div className="polaroid">
          <img src="https://t3.ftcdn.net/jpg/01/25/32/54/360_F_125325463_rXupAc7os8sxMfLWw4bMMqqnMCN6B89s.jpg" />
          <div className="caption"> Open Shootouts</div>
        </div>
      </div>
      <div onClick={() => navigate('review')} className="item">
        <div className="polaroid">
          <img src="https://s3.amazonaws.com/cobbcounty.org.if-us-east-1/s3fs-public/styles/thumbnail_263x194/public/2018-07/Court-Gavel-Thumb.jpg?itok=c2UcIq5k" />
          <div className="caption">Under Jury's Eye</div>
        </div>
      </div>
      <div onClick={() => navigate('finished')} className="item">
        <div className="polaroid">
          <img src="https://media.istockphoto.com/id/1191680341/vector/best-golden-cup-star-winner-stage-podium-scene-with-for-award-ceremony-on-night-background.jpg?s=612x612&w=0&k=20&c=LLlcTiRVSYF68ThIW4Tg9ao5j-Q7a-xfwAyGvQJlu8o=" />
          <div className="caption">The Ultimate Star </div>
        </div>
      </div>
    <div onClick={() => navigate('/AllStars')} className="item">
        <div className="polaroid">
          <img src="https://www.freevector.com/uploads/vector/preview/12509/FreeVector-Party-Crowd.jpg" />
          <div className="caption">All Stars </div>
        </div>
      </div>
     {userData?.role === 2 && <div onClick={() => navigate('/AdminPanel')} className="item">
        <div className="polaroid">
          <img src="https://hackaday.com/wp-content/uploads/2018/03/gears-featured.jpg?w=800" />
          <div className="caption">Admin </div>
        </div>
      </div>}
      {userData?.role === 1 && userData?.participations !== undefined ? (
        <div onClick={() => navigate('/MyShots')} className="item">
          <div className="polaroid">
            <img src="https://daily.jstor.org/wp-content/uploads/2017/11/james_bond_title_header_1050x700.png" />
            <div className="caption">My shots</div>
          </div>
        </div>
      ) : null}
      
        <div onClick={() => navigate(`/profile/${userData?.username}`)} className="item">
          <div className="polaroid">
            <img src="https://i.pinimg.com/736x/d8/af/18/d8af18af9b2bc46b408bbe3a7329d9ff.jpg" />
            <div className="caption">My space</div>
          </div>
        </div>    
    </div>
  )
}
