import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry'

import { AppContext } from '../../context/app.context'
import { getParticipationById } from '../../services/participation.services'
import { useContext } from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'

export const MyShotsView = () => {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)
  const [myParticipations, setMyParticipations] = useState([])

  const navigate = useNavigate()
  useEffect(() => {
    if (userData)
      Promise.all(Object.keys(userData?.participations)?.map((part) => getParticipationById(part)))
        .then(setMyParticipations)
        .catch((e) => console.log(e.message))
  }, [userData])

  // console.log(myParticipations)

  return (
    <div className="p-10">
      <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}>
        <Masonry gutter="20px">
          {myParticipations.map((item) => (
            <img
              key={item.id}
              src={item.photo}
              className="w-full block cursor-pointer border-4 border-secondary"
              onClick={() =>
                navigate(`/dashboard/:phase/contest:contestId/participation${item.id}`)
              }
            />
          ))}
        </Masonry>
      </ResponsiveMasonry>
    </div>
  )
}
