import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { getUser, updateUserProfilePic } from '../../services/user.service'
import { useContext, useEffect, useState } from 'react'

import { AppContext } from '../../context/app.context'
import ProfileInfo_1 from '../../components/ProfileGroup/ProfileInfo_1'
import ProfileInfo_2 from '../../components/ProfileGroup/ProfileInfo_2'
import Slides from '../../components/ProfileGroup/Slides'
import { checkRankByPoints } from '../../utils/helpers'
import { getParticipationById } from '../../services/participation.services'
import { storage } from '../../firebase/config'
import { useParams } from 'react-router-dom'

export default function Profile() {
  const { user, userData, setAppState, addToast } = useContext(AppContext)
  const [myParticipations, setMyParticipations] = useState([])
  const [currentUser, setCurrentUser] = useState()

  const { username } = useParams()
  const currentUserName = username ?? userData.username
  useEffect(() => {
    getUser(currentUserName)
      .then(setCurrentUser)
      .catch((e) => console.log(e.message))
  }, [currentUserName])

  useEffect(() => {
    if (currentUser?.role === 1 && currentUser?.participations !== undefined)
      Promise.all(
        Object.keys(currentUser?.participations)?.map((part) => getParticipationById(part))
      )
        .then(setMyParticipations)
        .catch((e) => console.log(e.message))
  }, [currentUser])

  // console.log(myParticipations)
  const uploadPicture = (e) => {
    e.preventDefault()

    const file = e.target[0]?.files[0]

    if (!file) return addToast('error', 'Please select file!')

    const picture = ref(storage, `images/${username}/avatar`)

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateUserProfilePic(username, url).then(() => {
            setAppState({
              user,
              userData: {
                ...userData,
                avatarUrl: url,
              },
            })
          })
        })
      })
      .catch(console.error)
  }

  return (
    <div className="flex lg:flex-row flex-col min-h-full lg:justify-center lg:items-start justify-start items-center lg:mt-28 mt-4">
      <div className="lg:w-[40%] w-full flex flex-col  justify-center items-center lg:my-0 lg:mx:0 mx-2">
        <ProfileInfo_2
          currentUser={currentUser}
          userData={userData}
          checkRankByPoints={checkRankByPoints}
        />
        <ProfileInfo_1
          currentUser={currentUser}
          userData={userData}
          uploadPicture={uploadPicture}
        />
      </div>

      <div className="lg:w-[60%] w-full flex justify-center items-start lg:my-0">
        {currentUser?.role === 1 && <Slides slides={myParticipations} />}
      </div>
    </div>
  )
}
