import { useContext, useState } from 'react'

import { AppContext } from '../../context/app.context'
import { ContestInfo } from '../../components/ContestInfo/ContestInfo'
import PhotoGrid from '../../components/PhotoGrid/PhotoGrid'
import { getContestById } from '../../services/contest.services'
import { getParticipationById } from '../../services/participation.services'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const SingleContestViewFinished = () => {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)

  const { contestId } = useParams()
  const [myParticipation, setMyParticipation] = useState('')
  const [contest, setContest] = useState({})

  const phase = 'finished'
  const [isJury, setIsJury] = useState(false)

  useEffect(() => {
    getContestById(contestId)
      .then(setContest)
      .catch((error) => {
        console.log(error.message)
      })

    if (contest?.juryList?.[userData?.username] || userData?.role == 2) setIsJury(true)
  }, [userData])

  useEffect(() => {
    getParticipationById(contest?.participations?.[userData?.username])
      .then(setMyParticipation)
      .catch((error) => {
        console.log(error.message)
      })
  }, [contest])
// console.log(myParticipation);
  return (
    <div>
      <ContestInfo phase={phase} contest={contest} />
      <PhotoGrid contestId={contestId} finished={true} />
    </div>
  )
}
