import './ContestList.css'

import { useContext, useEffect } from 'react'

import { AppContext } from '../../context/app.context'
import ContestCard from '../../components/ContestCard/ContestCard'
import InfoContests from '../../components/ContestsListGroup/InfoContests'
import Pagination from '../../components/ContestsListGroup/Pagination'
import { getAllContests } from '../../services/contest.services'
import { useParams } from 'react-router-dom'
import { useState } from 'react'

export default function ContestList() {
  const { userData } = useContext(AppContext)
  const { phase } = useParams()
  const [contests, setContests] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(8)
  const lastPostIndex = currentPage * postsPerPage
  const firstPostIndex = lastPostIndex - postsPerPage
  const currentPosts = contests.slice(firstPostIndex, lastPostIndex)
  useEffect(() => {
    getAllContests()
      .then((allContests) => {
        switch (phase) {
          case 'registration':
            setContests(allContests.filter((contest) => contest.phaseOneEndTime > Date.now()))
            break
          case 'review':
            setContests(
              allContests
                .filter(
                  (contest) =>
                    contest.phaseOneEndTime < Date.now() && contest.phaseTwoEndTime > Date.now()
                )
                .filter((contest) => Object.keys(contest?.juryList).includes(userData?.username))
            )
            break
          case 'finished':
            setContests(allContests.filter((contest) => contest.phaseTwoEndTime < Date.now()))
            break
        }
      })
      .catch((e) => console.log(e.message))
  }, [])

  return (
    <div className="mb-8">
      <InfoContests phase={phase} />
      <div className="flex flex-wrap items-start justify-center">
        {currentPosts.map((contest) => (
          <ContestCard key={contest.contestId} contest={contest} />
        ))}
      </div>
      <Pagination
        totalPosts={contests.length}
        postsPerPage={postsPerPage}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </div>
  )
}

//SLIDER
// import { MdChevronLeft, MdChevronRight } from 'react-icons/md'
// const sliderLeft = () => {
//   let slider = document.getElementById('slider')
//   slider.scrollLeft = slider.scrollLeft - 500
// }

// const sliderRight = () => {
//   let slider = document.getElementById('slider')
//   slider.scrollLeft = slider.scrollLeft + 500
// }

{
  /* <MdChevronLeft
          className="opacity-50 cursor-pointer hover:opacity-100"
          onClick={sliderLeft}
          size={40}
        /> */
}

{
  /* <MdChevronRight
          className="opacity-50 cursor-pointer hover:opacity-100"
          onClick={sliderRight}
          size={40}
        /> */
}
