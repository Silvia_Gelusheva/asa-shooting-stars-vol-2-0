import { AppContext } from '../../context/app.context'
import { ContestInfo } from '../../components/ContestInfo/ContestInfo'
import { JunkieParticipationView } from '../JunkieParticipationView/JunkieParticipationView'
import PhotoGrid from '../../components/PhotoGrid/PhotoGrid'
import { getContestById } from '../../services/contest.services'
import { useContext } from 'react'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useState } from 'react'

export const SingleContestViewRegistrationPhase = () => {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)
  const { contestId } = useParams()
  const phase = 'registration'
  const [contest, setContest] = useState({})
  const [isJury, setIsJury] = useState('false')

  useEffect(() => {
    getContestById(contestId)
      .then(setContest)
      .catch((error) => {
        console.log(error.message)
      })
    if (contest?.juryList?.[userData?.username] || userData?.role == 2) setIsJury(true)
  }, [userData])

  return (
    <div>
      <ContestInfo phase={phase} contest={contest} />
      {isJury === true ? (
        <PhotoGrid contestId={contestId} />
      ) : (
        <JunkieParticipationView contest={contest} username={userData?.username} />
      )}
    </div>
  )
}
