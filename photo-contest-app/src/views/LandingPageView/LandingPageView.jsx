// import {GiCursedStar} from 'react-icons/gi'
import './LandingPageView.css'

import { AppContext } from '../../context/app.context'
import { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

export default function LandingPageView() {
  const { addToast, userData, username, setAppState, ...appState } = useContext(AppContext)

  const handleClick = () => {
    userData?.username ? navigate('dashboard') : navigate('login')
  }
  const navigate = useNavigate()
  return (
    <div className="flex lg:flex-row flex-col justify-center min-h-full lg:items-start items-center ">
      <div className="flex">
        <img
          className="lg:h-[100%] h-48 mix-blend-multiply"
          src="https://eavf3cou74b.exactdn.com/wp-content/uploads/2019/10/24161400/man-why-do-people-become-photographers.jpg?strip=all&lossy=1&resize=443%2C600&ssl=1"
          alt=""
        />
      </div>

      <div className="flex flex-col place-items-center lg:mt-20">
        <p className="heading font-bold lg:text-[70px] text-3xl tracking-wide lg:mt-10 mb-5 mt-5">
          ASA SHOOTING STARS
        </p>

        <div className=" flex flex-col items-center container-card lg:mt-16 lg:mr-0 lg:ml-0 mt-10 mr-8 ml-8">
          <div>
            <span className="intro intro--the">The</span>
            <span className="intro intro--num">No #1</span>
            <span className="intro">vintage photo battle place</span>
          </div>

          <div className="pointer mt-20">
            <h4
              className="font-semibold text-center pt-2 pl-4 cursor-pointer"
              onClick={handleClick}>
              Get Started
            </h4>
          </div>
        </div>
      </div>
    </div>
  )
}
