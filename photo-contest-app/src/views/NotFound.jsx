export const NotFound = () => {
  return (
    <h1 className="text-center">
      404 <br />
      Page not found
    </h1>
  )
}
