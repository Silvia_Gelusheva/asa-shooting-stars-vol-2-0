import { AppContext } from '../../context/app.context'
import { loginUser } from '../../services/auth.service'
import { useContext } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'

export default function Login() {
  const { addToast, setAppState, ...appState } = useContext(AppContext)
  const navigate = useNavigate()
  const form = useForm()
  const { register, handleSubmit } = form

  const onSubmit = async (data) => {
    try {
      const credentials = await loginUser(data.email, data.password)

      setAppState({
        ...appState,
        user: {
          email: credentials?.user?.email,
          uid: credentials?.user?.uid,
        },
      })
      addToast('success', 'You have been logged!')

      navigate('/dashboard')
    } catch (error) {
      addToast('accent', error.message)
    }
  }

  return (
    <div className="flex items-center justify-center min-h-full bg-inherit">
      <div className="flex flex-col justify-center items-center gap-6 bg-inherit mt-20">
        <div className="text-center">
          <p className="pb-1 text-4xl font-bold text-primary">Hello there!</p>
          <p className="py-1 text-primary text-opacity-80">
            Please enter your details to log in to join our contests, or just enjoy all the
            beautiful pictures & give your support to our Shooting Stars!
          </p>
          <h2 className="text-xl font-bold text-primary mt-4">Do not have an account?</h2>
          <button
            className="btn btn-md btn-secondary shadow-md border-1 shadow-black text-primary mt-4"
            onClick={() => navigate('/register')}>
            Register
          </button>
        </div>

        <form
          className="flex justify-center rounded-md mb-20 max-w-full shadow-2xl shadow-black lg:min-w-[450px] bg-primary"
          onSubmit={handleSubmit(onSubmit)}>
          <div className="card-body gap-0">
            <h1 className="text-2xl font-bold text-neutral mb-8 text-center font-sans">
              MEMBER LOGIN
            </h1>
            <div className="form-control gap-4">
              {/* form email */}
              <input
                id="email"
                type="email"
                {...register('email')}
                placeholder="email"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="email">
               
              </label>
            </div>

            <div className="form-control">
              
              {/* form password */}
              <input
                id="password"
                type="password"
                {...register('password')}
                placeholder="password"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="password">
                
              </label>
            </div>
            <div className="form-control mt-6">
              <button className="btn btn-accent text-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}
