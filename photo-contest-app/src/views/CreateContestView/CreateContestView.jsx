import './CreateContestView.css'

import { HOUR_IN_MILLISECONDS, ORGANISERS } from '../../common/constants'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { useContext, useState } from 'react'

import { AppContext } from '../../context/app.context'
import ContestTitle from '../../components/CreateContestGroup/ContestTitle'
import InviteJunkies from '../../components/CreateContestGroup/InviteJunkies'
import InviteJury from '../../components/CreateContestGroup/InviteJury'
import SetPhaseDuration from '../../components/CreateContestGroup/SetPhaseDuration'
import UploadCoverPhoto from '../../components/CreateContestGroup/UploadCoverPhoto'
import { createContest } from '../../services/contest.services'
import { getAllUsers } from '../../services/user.service'
import { storage } from '../../firebase/config'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { v4 } from 'uuid'

export default function CreateContestView() {
  const { addToast } = useContext(AppContext)

  const openedModalClass = 'modal modal-open modal-bottom sm:modal-middle'
  const closedModalClass = 'modal modal-bottom sm:modal-middle'

  // Invite participants(Junkies)
  const [junkiesModalClass, setJunkiesModalClass] = useState(closedModalClass)
  const [invitationalChecked, setInvitationalChecked] = useState(false)
  const inviteListActiveClass = 'btn btn-primary text-white'
  const inviteListDisabledClass = 'btn btn-disabled bg-inherit'
  const [inviteListBtnClass, setInviteListBtnClass] = useState(inviteListDisabledClass)

  const toggleInvitationalMode = () => {
    if (invitationalChecked === false) {
      setInvitationalChecked(true)
      setInviteListBtnClass(inviteListActiveClass)
    } else {
      setInvitationalChecked(false)
      setInviteListBtnClass(inviteListDisabledClass)
    }
  }
  const openInviteJunkiesList = () => {
    setJunkiesModalClass(openedModalClass)
  }
  const closeInviteJunkiesList = () => {
    setJunkiesModalClass(closedModalClass)
  }

  const [inviteList, setInviteList] = useState({})
  const addToInviteList = (username) => {
    setInviteList({ ...inviteList, [username]: true })
  }
  const removeFromInviteList = (username) => {
    delete inviteList[username]
    setInviteList({ ...inviteList })
  }

  // Invite Jury
  const [juryModalClass, setJuryModalClass] = useState(closedModalClass)
  const [guestJuryChecked, setGuestJuryChecked] = useState(false)
  // const inviteListActiveClass = 'btn btn-primary'
  // const inviteListDisabledClass = 'btn btn-disabled'
  const [inviteJuryBtnClass, setInviteJuryBtnClass] = useState(inviteListDisabledClass)

  const toggleInviteJuryMode = () => {
    if (guestJuryChecked === false) {
      setGuestJuryChecked(true)
      setInviteJuryBtnClass(inviteListActiveClass)
    } else {
      setGuestJuryChecked(false)
      setInviteJuryBtnClass(inviteListDisabledClass)
    }
  }
  const openGuestJuryList = () => {
    setJuryModalClass(openedModalClass)
  }
  const closeGuestJuryList = () => {
    setJuryModalClass(closedModalClass)
  }

  // Invite List (Guest Jury)
  const [guestJuryList, setGuestJuryList] = useState({ ...ORGANISERS })

  const addToGuestJuryList = (username) => {
    setGuestJuryList({ ...guestJuryList, [username]: true })
  }
  const removeFromGuestJuryList = (username) => {
    delete guestJuryList[username]
    setGuestJuryList({ ...guestJuryList })
  }

  // Load Junkies
  const [allUsers, setAllUsers] = useState([])

  useEffect(() => {
    getAllUsers()
      .then(setAllUsers)
      .catch((e) => {
        console.log(e.message)
      })
  }, [])
  const allJunkies = allUsers?.filter((user) => user.role === 1).sort((a, b) => b.points - a.points)

  // Create Contest
  const [title, setTitle] = useState('')
  const [category, setCategory] = useState('')
  const [cover, setCover] = useState('')
  const [phase1Duration, setPhase1Duration] = useState(0)
  const [phase2Duration, setPhase2Duration] = useState(0)
  const [coverUrl, setCoverUrl] = useState('')

  const navigate = useNavigate()

  const uploadPicture = async () => {
    const file = cover
    if (!file) return addToast('error', 'Please select file!')

    const coverRef = ref(storage, `/coverPhotos/${v4()}`)

    try {
      const result = await uploadBytes(coverRef, file)
      const url = await getDownloadURL(result.ref)
      return url
    } catch (error) {
      addToast('error', error.message)
    }
  }

  const submitContest = async (title, category, phase1Duration, phase2Duration, guestJuryList) => {
    if (title.length < 2) return addToast('error', 'Invalid Title')
    if (category.length < 2) return addToast('error', 'Invalid Category')
    if (phase1Duration < 1 || phase1Duration > 31)
      return addToast('error', 'Invalid Duration for phase 1')
    if (phase2Duration < 1 || phase2Duration > 24)
      return addToast('error', 'Invalid Duration for phase 2')

    const createdOn = Date.now()

    const phase1EndTime = createdOn + phase1Duration * 24 * HOUR_IN_MILLISECONDS
    const phase2EndTime = phase1EndTime + phase2Duration * HOUR_IN_MILLISECONDS

    const url = await uploadPicture()

    try {
      await createContest(
        createdOn,
        title,
        category,
        url,
        phase1EndTime,
        phase2EndTime,
        guestJuryList,
        invitationalChecked,
        inviteList
      )
      addToast('success', 'Contest created!')

      navigate('/dashboard')
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className="min-h-full flex flex-col justify-start items-center lg:mx-0 mx-6">
      {/* Banner */}
      <div className="flex mt-10 lg:mb-4 mb-10 justify-center">
        <h5 className="lg:text-2xl text-lg shadow-lg px-20">- New Contest -</h5>
      </div>

      {/* main container */}
      <div className="flex lg:flex-row flex-col justify-center items-center w-full gap-6">
        {/* upload cover */}
        <div className="flex items-center justify-center container container-card lg:m-10 h-[360px] ">
          <UploadCoverPhoto setCover={setCover} coverUrl={coverUrl} setCoverUrl={setCoverUrl} />
        </div>

        {/* title, category, deadline */}
        <div className="flex items-center justify-center container container-card lg:m-10 h-[360px] ">
          <div className=" px-8 pt-6 pb-8 mb-4">
            <ContestTitle
              title={title}
              setTitle={setTitle}
              category={category}
              setCategory={setCategory}
            />

            <SetPhaseDuration
              phase1Duration={phase1Duration}
              setPhase1Duration={setPhase1Duration}
              phase2Duration={phase2Duration}
              setPhase2Duration={setPhase2Duration}
            />
          </div>
        </div>

        <div className="flex items-center justify-center container container-card lg:m-10 h-[360px] ">
          <div>
            <div className="flex flex-row gap-4 justify-start items-center">
              {/* Invite Junkies */}
              <InviteJunkies
                allJunkies={allJunkies}
                toggleInvitationalMode={toggleInvitationalMode}
                invitationalChecked={invitationalChecked}
                openInviteJunkiesList={openInviteJunkiesList}
                inviteListBtnClass={inviteListBtnClass}
                junkiesModalClass={junkiesModalClass}
                closeInviteJunkiesList={closeInviteJunkiesList}
                addToInviteList={addToInviteList}
                removeFromInviteList={removeFromInviteList}
              />

              {/* Invite Jury */}
              <InviteJury
                allJunkies={allJunkies}
                toggleInviteJuryMode={toggleInviteJuryMode}
                guestJuryChecked={guestJuryChecked}
                openGuestJuryList={openGuestJuryList}
                inviteJuryBtnClass={inviteJuryBtnClass}
                juryModalClass={juryModalClass}
                closeGuestJuryList={closeGuestJuryList}
                addToGuestJuryList={addToGuestJuryList}
                removeFromGuestJuryList={removeFromGuestJuryList}
              />
            </div>
          </div>
        </div>
      </div>
      {/* submit contest */}
      <div className="flex justify-center lg:justify-end w-full lg:mr-36">
        <button
          onClick={() =>
            submitContest(title, category, phase1Duration, phase2Duration, guestJuryList)
          }
          className="my-4 btn hover:bg-secondary shadow-lg shadow-black bg-primary text-secondary hover:border-secondary hover:text-gray-800 text-center font-semibold ">
          SUBMIT
        </button>
      </div>
    </div>
  )
}
