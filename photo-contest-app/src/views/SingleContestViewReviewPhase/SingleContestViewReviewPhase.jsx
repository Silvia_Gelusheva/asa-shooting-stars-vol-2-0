import { useContext, useState } from 'react'

import { AppContext } from '../../context/app.context'
import { ContestInfo } from '../../components/ContestInfo/ContestInfo'
import PhotoGrid from '../../components/PhotoGrid/PhotoGrid'
import { getContestById } from '../../services/contest.services'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const SingleContestViewReviewPhase = () => {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)

  const phase = 'review'

  const { contestId } = useParams()

  const [contest, setContest] = useState({})

  const [isJury, setIsJury] = useState('false')

  useEffect(() => {
    getContestById(contestId)
      .then(setContest)
      .catch((error) => {
        console.log(error.message)
      })
    if (contest?.juryList?.[userData?.username] || userData?.role == 2) setIsJury(true)
  }, [userData])

  return (
    <div>
      <ContestInfo contest={contest} phase={phase} />
      <PhotoGrid contestId={contestId} />
    </div>
  )
}
