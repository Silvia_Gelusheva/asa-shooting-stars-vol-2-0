import { createUser, getUser } from '../../services/user.service'

import { AppContext } from '../../context/app.context'
import { registerUser } from '../../services/auth.service'
import { useContext } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'

export default function Register() {
  const { addToast, userData, setAppState, ...appState } = useContext(AppContext)
  const navigate = useNavigate()
  const form = useForm()
  const { register, handleSubmit } = form

  const onSubmit = async (data) => {
    const user = await getUser(data.username)

    if (user !== null) return addToast('error', `Username ${data.username} already exists!`)

    const credentials = await registerUser(data.email, data.password)

    try {
      const userData = await createUser(
        credentials.user.uid,
        data?.firstname,
        data?.lastname,
        data?.username
      )

      setAppState({
        ...appState,
        userData,
      })

      addToast('success', 'You have registered successfully!')

      navigate('/login')
    } catch (error) {
      return console.log(error.message)
    }
  }

  return (
    <div className="flex items-start justify-center min-h-full bg-inherit pt-20">
      <div className="flex flex-wrap justify-evenly items-center lg:gap-48">
        <div className="text-center lg:text-center">
          <p className=" py-2 text-3xl font-bold text-gray-800">Welcome to ASA Shooting Stars!</p>
          <p className=" text-gray-800 text-opacity-80">
            Enter your details to register, then log in, and take your shot!
          </p>
          <div className="flex justify-center items-start">
            <img src="./reg-image.jpg" alt="photographer" className="h-[100%] mix-blend-multiply" />
          </div>
        </div>
        <div className="flex justify-center rounded-md mb-20 max-w-full shadow-2xl shadow-black lg:min-w-[450px] bg-primary">
          <form className="card-body gap-2" onSubmit={handleSubmit(onSubmit)}>
            <h1 className="text-2xl font-bold text-neutral mb-8 text-center font-sans">REGISTER</h1>

            {/* form username */}
            <div className="form-control">
              <input
                id="username"
                type="username"
                {...register('username')}
                placeholder="username"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="username"></label>
            </div>

            {/* form first name */}
            <div className="form-control">
              <input
                id="firstname"
                type="firstname"
                {...register('firstname')}
                placeholder="first name"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="firstname"></label>
            </div>
            {/* form last name */}
            <div className="form-control">
              <input
                id="lastname"
                type="lastname"
                {...register('lastname')}
                placeholder="last name"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="lastname"></label>
            </div>

            <div className="form-control">
              {/* form email */}
              <input
                id="email"
                type="email"
                {...register('email')}
                placeholder="email"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="email"></label>
            </div>

            <div className="form-control">
              {/* form password */}
              <input
                id="password"
                type="password"
                {...register('password')}
                placeholder="password"
                className="input bg-gray-200 text-primary"
              />
              <label className="label" htmlFor="password"></label>
            </div>
            <div className="form-control">
              <button className="btn btn-accent text-primary font-bold">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}
