import ContestTable from '../../components/ContestsTable/ContestsTable'
import Junkies from '../../components/ContestsTable/Junkies'
import { JunkiesTable } from '../../components/JunkiesTable/JunkiesTbale'

export default function AdminPanel() {
  return (
    <div className="flex flex-col md:flex-col flex-nowrap lg:flex-row justify-around w-full">
      <ContestTable />
      {/* <Junkies /> */}
    </div>
  )
}
