import { enrollParticipant, getParticipationById } from '../../services/contest.services'
import { useEffect, useState } from 'react'

import Enroll from '../../components/Enroll/Enroll'
import PhotoUpload from '../../components/PhotoUpload/PhotoUpload'
import { SimpleJunkieParticipation } from '../../components/SimpleJunkieParticipation/SimpleJunkieParticipation'
import { useNavigate } from 'react-router-dom'

export const JunkieParticipationView = ({ username, contest }) => {
  const [enrollList, setEnrollList] = useState([])
  const [myParticipation, setMyParticipation] = useState('')
  const [participation, setParticipation] = useState({})
  const navigate = useNavigate()
  useEffect(() => {
    setEnrollList(contest?.enrolled)
    setMyParticipation(contest?.participations?.[username])
  }, [contest])

  useEffect(() => {
    getParticipationById(myParticipation)
      .then(setParticipation)
      .catch((error) => console.log(error.message))
  }, [myParticipation])

  // console.log(myParticipation)

  const handleBack = () => {
    navigate('/dashboard/registration/')
  }
  const handleEnroll = async () => {
    await enrollParticipant(contest?.contestId, username)
    setEnrollList([...enrollList, username])
  }

  return (
    <div className="flex justify-center items-center">
      {myParticipation !== undefined ? (
        <SimpleJunkieParticipation
          photo={participation?.photo}
          author={participation?.author}
          title={participation?.title}
          story={participation?.story}
          addedOn={participation?.addedOn}
        />
      ) : enrollList?.includes(username) && myParticipation === undefined ? (
        <PhotoUpload />
      ) : (
        <Enroll handleBck={handleBack} handleEnroll={handleEnroll} />
      )}
    </div>
  )
}
