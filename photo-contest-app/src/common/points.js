export const points = {
  joinOpenContest: 1,
  invitedToContest: 3,
  thirdPlace: 20,
  thirdPlaceShared: 10,
  secondPlace: 35,
  secondPlaceShared: 25,
  firstPlace: 50,
  firstPlaceShared: 40,
}

// Finishing at 1st place with double the score of the 2nd (e.g., 1st has been awarded 8.6 points average, and 2nd is 4.3 or less) – 75 points
