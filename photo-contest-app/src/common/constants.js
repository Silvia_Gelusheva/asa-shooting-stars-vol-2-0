export const USERNAME_MIN_LENGTH = 3
export const USERNAME_MAX_LENGTH = 30
export const USER_FIRST_NAME_MIN_LENGTH = 1
export const USER_FIRST_NAME_MAX_LENGTH = 30
export const USER_LAST_NAME_MIN_LENGTH = 1
export const USER_LAST_NAME_MAX_LENGTH = 30
export const PHONE_NUMBER_LENGTH = 10

export const CONTEST_TITLE_MIN_LENGTH = 1
export const CONTEST_TITLE_MAX_LENGTH = 60
export const PHOTO_TITLE_MIN_LENGTH = 1
export const PHOTO_TITLE_MAX_LENGTH = 30

export const PHOTO_STORY_MIN_LENGTH = 10
export const PHOTO_STORY_MAX_LENGTH = 600

export const PASSWORD_MIN_LENGTH = 6
export const PASSWORD_MAX_LENGTH = 16

export const EMAIL_MIN_LENGTH = 6
export const EMAIL_MAX_LENGTH = 100

export const HOUR_IN_MILLISECONDS = 60 * 60 * 1000

export const ORGANISERS = { anatoli: true, ang7: true, Silvia: true }

export const DEFAULT_SCORE = 3
export const INVITATIONAL_SCORE = 3
export const ENROLL_SCORE = 1

export const THIRD_PLACE = 20
export const THIRD_PLACE_SHARED = 10
export const SECOND_PLACE = 35
export const SECOND_PLACE_SHARED = 25
export const FIRST_PLACE_DOUBLE_SCORES = 75
export const FIRST_PLACE = 50
export const FIRST_PLACE_SHARED = 40

export const UNSPLASH_SEARCH_PHOTOS_APP_KEY = 'q5s7rZnH7B62MNQv6QNFhHvFEYb7yqYLWWuW6meKDVA'
