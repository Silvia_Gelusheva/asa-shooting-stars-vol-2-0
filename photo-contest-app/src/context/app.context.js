import { createContext } from 'react'

export const AppContext = createContext({
  addToast() { },
  setAppState() { },
  theme: 'light',
  user: null,
  userData: null,
})
