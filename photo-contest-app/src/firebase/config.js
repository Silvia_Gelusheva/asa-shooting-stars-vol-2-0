import { getAuth } from 'firebase/auth'
import { getDatabase } from 'firebase/database'
import { getStorage } from 'firebase/storage'
import { initializeApp } from 'firebase/app'

const firebaseConfig = {
	apiKey: "AIzaSyDC7ScZB_i9WeYP6Zgma2xjxlwjvMcdfdU",
	authDomain: "asa-app-7588b.firebaseapp.com",
	projectId: "asa-app-7588b",
	storageBucket: "asa-app-7588b.appspot.com",
	messagingSenderId: "784172398990",
	appId: "1:784172398990:web:b36278a8e0c355aec80cdf",
	databaseURL: ' https://asa-app-7588b-default-rtdb.europe-west1.firebasedatabase.app',
}

export const app = initializeApp(firebaseConfig)
export const auth = getAuth(app)
export const db = getDatabase(app)
export const storage = getStorage(app)
