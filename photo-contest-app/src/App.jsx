import './App.css'

import { Route, Routes } from 'react-router'
import {
  accumulateUsersPoints,
  addRankingPointsFromContest,
  evaluateContest,
} from './utils/evaluate-scoring'
import { useEffect, useState } from 'react'

import AdminPanel from './views/AdminPanel/AdminPanel'
import { AppContext } from './context/app.context'
import { BrowserRouter } from 'react-router-dom'
import ContestsList from './views/ContestsList/ContestsList'
import CreateContestView from './views/CreateContestView/CreateContestView'
import { Dashboard } from './views/Dashboard/Dashboard'
import Header from './components/Header/Header'
import { JunkiesTable } from './components/JunkiesTable/JunkiesTbale'
import LandingPageView from './views/LandingPageView/LandingPageView'
import Login from './views/Login/Login'
import { MyShotsView } from './views/MyShotsView/MyShotsView'
import { NotFound } from './views/NotFound'
import Profile from './views/Profile/Profile'
import Register from './views/Register/Register'
import { SingleContestViewFinished } from './views/SingleContestViewFinished/SingleContestViewFinished'
import { SingleContestViewRegistrationPhase } from './views/SingleContestViewRegistrationPhase/SingleContestViewRegistrationPhase'
import { SingleContestViewReviewPhase } from './views/SingleContestViewReviewPhase/SingleContestViewReviewPhase'
import SingleParticipationView from './views/SingleParticipationView/SingleParticipationView'
import { UnsplashSearch } from './components/UnsplashSearch/UnsplashSearch'
import { auth } from './firebase/config'
import { getAllContests } from './services/contest.services'
import { getUserById } from './services/user.service'
import { useAuthState } from 'react-firebase-hooks/auth'

function App() {
  useEffect(() => {
    // Evaluate finished contests
    getAllContests().then((result) =>
      result
        .filter((contest) => contest?.phaseTwoEndTime < Date.now())
        .map((contest) => {
          evaluateContest(contest)
          return contest
        })
        .map((contest) => addRankingPointsFromContest(contest))
    )
  }, [])

  const [user] = useAuthState(auth)

  const [appState, setAppState] = useState({
    // theme: localStorage.getItem('theme') || 'light',
    user: user ? { email: user.email, uid: user.uid } : null,
    userData: null,
  })

  /**
   * @type {[Array<{class: string, message: string}>, Function]}
   */

  const [toast, setToasts] = useState([])

  useEffect(() => {
    setAppState({
      ...appState,
      user: user ? { email: user.email, uid: user.uid } : null,
    })
  }, [user])

  useEffect(() => {
    if (appState.user !== null && appState.userData === null) {
      getUserById(appState.user.uid)
        .then((userData) => setAppState((appState) => ({ ...appState, userData })))
        .catch((error) => addToast('error', error.message))
    }
  }, [appState.user])

  /**
   * @param {'success' | 'error'} type
   * @param {string} message
   */
  const addToast = (type, message) => {
    const toast = {
      class: type === 'error' ? 'alert-error' : 'alert-success',
      message,
    }

    setToasts((toasts) => [...toasts, toast])

    setTimeout(() => {
      setToasts((toasts) => toasts.filter((t) => t !== toast))
    }, 6000)
  }

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setAppState, addToast }}>
        <div className="App">
          <Header />
          <div className="min-h-[75vh]">
            <Routes>
              <Route path="/" element={<LandingPageView />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/dashboard/:phase" element={<ContestsList />} />
              <Route path="/createContest" element={<CreateContestView />} />
              <Route path="/profile/:username" element={<Profile />} />
              <Route path="/MyShots" element={<MyShotsView />} />
               <Route path="/AllStars" element={<JunkiesTable />} />
               <Route path="/AdminPanel" element={<AdminPanel />} />
               
              <Route
                path="/dashboard/:phase/contest:contestId/participation:participationId"
                element={<SingleParticipationView />}
              />
              <Route path="/inspirations" element={<UnsplashSearch />} />
              {/* NEW */}
              <Route path="*" element={<NotFound />} />
              <Route
                path="/dashboard/registration/contest:contestId"
                element={<SingleContestViewRegistrationPhase />}
              />
              <Route
                path="/dashboard/review/contest:contestId"
                element={<SingleContestViewReviewPhase />}
              />
              <Route
                path="/dashboard/finished/contest:contestId"
                element={<SingleContestViewFinished />}
              />
            </Routes>
            <div className="toast">
              {toast.map((toast, index) => (
                <div key={index} className={`alert ${toast.class}`}>
                  <div>
                    <span className="text-lg">{toast.message}</span>
                  </div>
                </div>
              ))}
            </div>
          </div>
          {/* <Footer /> */}
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  )
}

export default App
