export function makeSymmetricalEnum(object) {
  return Object.keys(object).map((key) => {
    Object.defineProperty(object, object[key], {
      value: key,
      enumerable: false,
    })
  })
}

export const checkRankByPoints = (points = 0) => {
  if (points >= 0 && points <= 50) {
    return 'Junkie'
  } else if (points >= 51 && points <= 150) {
    return 'Enthusiast'
  } else if (points >= 151 && points <= 1000) {
    return 'Master'
  } else if (points >= 1001) {
    return 'Photo Dictator'
  }
}
