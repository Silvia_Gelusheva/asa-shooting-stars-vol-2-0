import {
  DEFAULT_SCORE,
  ENROLL_SCORE,
  FIRST_PLACE,
  FIRST_PLACE_DOUBLE_SCORES,
  FIRST_PLACE_SHARED,
  INVITATIONAL_SCORE,
  SECOND_PLACE,
  SECOND_PLACE_SHARED,
  THIRD_PLACE,
  THIRD_PLACE_SHARED,
} from '../common/constants'
import { getAllContests, getContestById, getParticipationById } from '../services/contest.services'
import {
  getRankingPointsDB,
  updateContestEvaluation,
  updateFinalParticipationScore,
  updateRankingPointsDB,
} from '../services/scoring.services'

import { async } from '@firebase/util'
import { getAllParticipationReviewsById } from '../services/review.services'
import { getAllParticipationsByContestId } from '../services/participation.services'

export const evaluateSingleParticipation = async (participationId, juryMembersNumber) => {
  const reviews = await getAllParticipationReviewsById(participationId)

  let totalScore = reviews.reduce((finalScore, el) => (finalScore += el.rating), 0)

  if (reviews.length < juryMembersNumber) {
    totalScore += (juryMembersNumber - reviews.length) * DEFAULT_SCORE
  }

  const finalScore = Number((totalScore / juryMembersNumber).toFixed(1))

  updateFinalParticipationScore(finalScore, participationId)

  return
}

//

export const evaluateContest = (contest) => {
  const juryMembersNumber = Object.keys(contest?.juryList).length

  return Object.values(contest?.participations).map((participationId) =>
    evaluateSingleParticipation(participationId, juryMembersNumber)
  )
 
}

//

export const addRankingPointsFromContest = async (contest) => {

  if (contest?.evaluated === 'true') return

  const participations = await Promise.all(
    Object.values(contest?.participations).map((partId) => getParticipationById(partId))
  )

  const groupedByPoints = participations.reduce((acc, el) => {
    if (acc[el.score] !== undefined) {
      acc[el.score].push(el.author)
    } else {
      acc[el.score] = [el.author]
    }
    return acc
  }, {})

  let firstPlace = []
  let secondPlace = []
  let thirdPlace = []

  const finalSort = Object.entries(groupedByPoints).sort((a, b) => b[0] - a[0])

  finalSort.map((el, index) => {
    if (index === 0) {
      firstPlace = [...el[1]]
    }
    if (index === 1) {
      secondPlace = [...el[1]]
    }
    if (index === 2) {
      thirdPlace = [...el[1]]
    }
  })

  // FIRST PLACE
  if (firstPlace.length === 1) {
    if (finalSort[0][0] >= finalSort[1][0]) {
      await accumulateUserPoints(FIRST_PLACE_DOUBLE_SCORES, firstPlace[0])
    } else {
      await accumulateUserPoints(FIRST_PLACE, firstPlace[0])
    }
  } else if (firstPlace.length > 1) {
    await accumulateUsersPoints(FIRST_PLACE_SHARED, firstPlace)
  }

  //SECOND PLACE
  if (secondPlace.length === 1) {
    await accumulateUserPoints(SECOND_PLACE, secondPlace[0])
  } else if (secondPlace.length > 1) {
    await accumulateUsersPoints(SECOND_PLACE_SHARED, secondPlace)
  }
  //THIRD PLACE
  if (thirdPlace.length === 1) {
    await accumulateUserPoints(THIRD_PLACE, thirdPlace[0])
  } else if (thirdPlace.length > 1) {
    await accumulateUsersPoints(THIRD_PLACE_SHARED, thirdPlace)
  }

  // PARTICIPATION POINTS
  const participationPoints = contest.invitational ? INVITATIONAL_SCORE : ENROLL_SCORE
  // Promise.all(Object.keys(contest.participations).map(user => {
  //   accumulatePoints(participationPoints, user)
  // }))


  await updateContestEvaluation(contest.contestId)
}

const accumulateUserPoints = async (points, user) => {
  const existingPointsBeforeContest = await getRankingPointsDB(user)
  let currentPoints = existingPointsBeforeContest + points
  updateRankingPointsDB(currentPoints, user)
}

export const accumulateUsersPoints = async (points, users) => {
  Promise.all(
    users.map((user) => {
      accumulateUserPoints(points, user)
    })
  )
}
