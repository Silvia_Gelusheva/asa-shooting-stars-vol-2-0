import { MdChevronLeft, MdChevronRight } from 'react-icons/md'

export default function PhotoSlider({data}) {
  
  const sliderLeft = () => {
    let slider = document.getElementById('slider')
    slider.scrollLeft = slider.scrollLeft - 500
  }

  const sliderRight = () => {
    let slider = document.getElementById('slider')
    slider.scrollLeft = slider.scrollLeft + 500
  }

  return (
    <div className="relative flex items-center">
      <MdChevronLeft
        className="opacity-50 cursor-pointer hover:opacity-100"
        onClick={sliderLeft}
        size={40}
      />
      <div
        id="slider"
        className="w-full h-full overflow-x-scroll scroll whitespace-nowrap scroll-smooth scrollbar-hide">
        {data.map((item) => (
          <img
            className="h-[220px] inline-block p-2 cursor-pointer hover:scale-105 ease-in-out duration-300"
            key={item.id}
            src={item.photo}
            alt="image"
          />
        ))}
      </div>
      <MdChevronRight
        className="opacity-50 cursor-pointer hover:opacity-100"
        onClick={sliderRight}
        size={40}
      />
    </div>
  )
}
