export default function Footer() {
  return (
    <footer className="footer footer-center p-4 bg-base-100 shadow-inner text-base-content ">
      <div>
        <p className="font-">Copyright © 2022 - All right reserved by ASA Shooting Stars</p>
      </div>
    </footer>
  )
}
