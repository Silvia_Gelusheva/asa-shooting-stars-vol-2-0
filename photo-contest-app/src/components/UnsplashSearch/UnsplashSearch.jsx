import './UnsplashSearch.css'

import { AiOutlineClear } from 'react-icons/ai'
import { CgSearchLoading } from 'react-icons/cg'
import { fetchPhotosFromUnsplash } from '../../services/unsplash.service'
import { useEffect } from 'react'
import { useState } from 'react'

export const UnsplashSearch = () => {
  const [photos, setPhotos] = useState([])
  const [query, setQuery] = useState('')
  const [submitted, setSubmitted] = useState(false)

  useEffect(() => {
    fetchPhotosFromUnsplash(query)
      .then(setPhotos)
      .catch((error) => console.log(error.message))

    setSubmitted(false)
  }, [submitted])

  const handleClr = () => {
    setQuery('')
    setPhotos('')
  }

  const handleSubmit = () => {
    setSubmitted(true)
  }

  const handleKeypress = (e) => {
    if (e.keyCode === 13) {
      handleSubmit()
    }
  }

  return (
    <div className="relative flex flex-col justify-center items-center mt-10">
      {/* search bar */}
      {/* <div className="flex justify-center">
        <div className="form-control  mt-10">
          <div className="input-group">
            <input
              type="text"
              placeholder="Search…"
              className="input input-bordered border-gray-800"
              value={query}
              onChange={(e) => setQuery(e.target.value)}
              onKeyDown={handleKeypress}
            />
            <button className="btn btn-square bg-gray-800 text-base-100" onClick={handleSubmit}>
              <CgSearchLoading />
            </button>
            <button
              className="btn btn-square bg-gray-800 text-base-100"
              onClick={() => handleClr()}>
              <AiOutlineClear />
            </button>
          </div>
        </div>
      </div> */}
      <div className="flex flex-col justify-center">
        <p className="text-lg font-semibold opacity-75">
          "Photography is a language more universal than words."
        </p>
        <p className="text-sm opacity-50">- Minor White -</p>
        <p className="text-sm opacity-50">American photographer</p>
      </div>

      <div className="relative text-primary mt-10">
        <input
          type="search"
          placeholder="Inspire..."
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          onKeyDown={handleKeypress}
          className="border-primary border-2 h-10 px-5 pr-10 rounded-full text-sm focus:outline-none"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-4 text-primary"
          onClick={handleSubmit}>
          <CgSearchLoading />
        </button>
      </div>

      {/* photo grid */}
      <div className="h-full min-h-[500px]">
        <ul className="list">
          {photos?.results?.map((photo) => (
            <li key={photo?.id} className="list-content" >
              <img src={photo?.urls?.regular} className="h-[250px] w-[250px]" />
              <p>{photo?.user?.name}</p>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}
