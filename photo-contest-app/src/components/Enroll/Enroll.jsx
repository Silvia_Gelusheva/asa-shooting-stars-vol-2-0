import { useEffect, useState } from 'react'

import  Loader  from '../../components/Loader/Loader'
import image from "../../../src/assets/cam2.jpg"

export default function Enroll({handleBck, handleEnroll}) {



  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    }, 5000)
  }, [])


  return (
    <div className="lg:flex justify-center items-center gap-10 mt-8">
         {loading ? <Loader/> :  <><div className="flex gap-2 justify-center">
        <button
          onClick={handleBck}
          className="btn bg-secondary text-primary hover:bg-accent shadow-lg shadow-black pt-4 pb-4 px-10 text-center font-semibold">
          back
        </button>
        <button
          onClick={handleEnroll}
          className="btn bg-secondary text-primary hover:bg-accent shadow-lg shadow-black pt-4 pb-4 px-8 text-center font-semibold">
          enroll
        </button>
      </div><img src={image} alt="stars" className="mix-blend-multiply" /></>}
        </div>
  )
}
