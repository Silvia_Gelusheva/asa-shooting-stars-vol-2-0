import { MdDateRange } from 'react-icons/md'
// import { MdPlace } from 'react-icons/md'
import StarRating from '../Review/StarRating'
import defaultAvatar from '../../assets/empty.jpg'
import { getAvatarByUsername } from '../../services/user.service'
import { useEffect } from 'react'
import { useState } from 'react'

export default function ParticipationInfo({ author, score, title, description, createdOn }) {
  const [avatar, setAvatar] = useState('')

  useEffect(() => {
    getAvatarByUsername(author)
      .then(setAvatar)
      .catch((error) => addToast('error', error.message))
  }, [author])

  return (
    <div className=" flex w-full">
      <div className="w-full max-w-screen-sm m-auto p-3 min-h-100 overflow-hidden">
        <div className="wrapper bg-accent opacity-75 flex flex-row p-3 border-4 border-gray-100 rounded-3xl shadow-md">
          <div className="w-1/6 flex-grow-0">
            <div className="rounded-full w-full h-auto border-gray-100 border-4 p-1 overflow-hidden">
              <img
                className="rounded-full w-full"
                src={avatar ? avatar : defaultAvatar}
                // src={review?.author?.avatarUrl}
                alt="avatar"
              />
            </div>
          </div>
          <div className="w-5/6 info text-left pl-3 text-black">
            <div className="written-by uppercase text-black tracking-wide text-sm">
              Photo shoot by
            </div>
            <div className="name font-bold py-1">{author}</div>
            <small>
              <span className="flex flex-row text-black justify-start">
                <div className="name text-lg font-bold">{title}</div>
                <StarRating score={score} />
              </span>
            </small>

            <div className="text-sm text-gray-800">
              {description}
              <small>
                <span className="flex flex-row text-black justify-start">
                  <MdDateRange />
                  <p>{createdOn}</p>
                </span>
              </small>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
