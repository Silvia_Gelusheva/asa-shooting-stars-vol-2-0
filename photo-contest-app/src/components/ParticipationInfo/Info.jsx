export default function Info() {
    return (
      <div
        className="hero min-h-screen"
        style={{
          backgroundImage: `url("https://images.hdqwalls.com/wallpapers/tree-of-life-4k-y5.jpg")`,
        }}
      >
        <div className="hero-overlay bg-opacity-20"></div>
        <section className="pt-16 bg-blueGray-50">
          <div className="w-full lg:w-full px-4 mx-auto">
            <div className="relative flex flex-col min-w-0 break-words bg-gray-200 w-full mb-6 shadow-xl rounded-lg mt-16">
              <div className="px-16">
                <div className="flex flex-wrap justify-center">
                  <div className="w-full px-4 flex justify-center">
                    <div className="relative">
                      <img
                        className="shadow-xl rounded-full h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px"
                        style={{ maxWidth: "150px" }}
                        src="https://cf.shopee.sg/file/f6b527c4f3dd83d7b524fefd3bc2f516"
                        alt="profile"
                      />
                    </div>
                  </div>
                  <div className="w-full px-4 text-center mt-20">
  
                      {/* statistics */}
                    <div className="flex justify-center py-4 lg:pt-4 pt-8">
                      <div className="mr-4 p-3 text-center">
                        <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                          20
                        </span>
                        <span className="text-sm text-blueGray-400">Posts</span>
                      </div>
                      <div className="lg:mr-4 p-3 text-center">
                        <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                          10
                        </span>
                        <span className="text-sm text-blueGray-400">Likes</span>
                      </div>
                      <div className="lg:mr-4 p-3 text-center">
                        <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                          15
                        </span>
                        <span className="text-sm text-blueGray-400">
                          Comments
                        </span>
                      </div>
                    </div>
                    {/* statistics */}
  
                  </div>
                </div>
                <div className="text-center mt-12">
                  <h3 className="text-xl font-semibold leading-normal mb-2 text-blueGray-700">
                    Silvis Gelusheva
                  </h3>
                  <div className="text-sm leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                    <i className="fas fa-map-marker-alt mr-2 text-lg text-blueGray-400"></i>
                    Sofia, Bulgaria
                  </div>
                  <div className="mb-2 text-blueGray-600 mt-10">
                    <i className="fas fa-briefcase mr-2 text-lg text-blueGray-400"></i>
                    Admin
                  </div>
                  <div className="mb-2 text-blueGray-600">
                    <i className="fas fa-university mr-2 text-lg text-blueGray-400"></i>
                    Telerik Academy
                  </div>
                </div>
                <div className="mt-10 py-10 border-t border-blueGray-200 text-center">
                  <div className="flex flex-wrap justify-center">
                    <div className="w-full lg:w-9/12 px-4">
                     
                     
                     
                   </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer className="relative  pt-8 pb-6 mt-8">
            <div className="container mx-auto px-4">
              <div className="flex flex-wrap items-center md:justify-between justify-center">
                <div className="w-full md:w-6/12 px-4 mx-auto text-center">
                  <div className="text-sm text-blueGray-500 font-semibold py-1">
                    {" "}
                    <a
                      href=""
                      className="text-blueGray-500 hover:text-gray-800"
                      target="_blank"
                    ></a>{" "}
                    <a
                      href=""
                      className="text-blueGray-500 hover:text-blueGray-800"
                      target="_blank"
                    >
                      {" "}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </section>
      </div>
    );
  }