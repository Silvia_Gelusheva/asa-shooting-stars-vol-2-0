import "./Notifications.css";
import {IoIosNotificationsOutline} from 'react-icons/io'
export default function Notifications() {
  return (
    <div>
      <button type="button" class="icon-button">
        <span class="bg-gray-200 rounded-full"><IoIosNotificationsOutline size={28}/></span>
        <span class="icon-button__badge">2</span>
      </button>
    </div>
  )
}
