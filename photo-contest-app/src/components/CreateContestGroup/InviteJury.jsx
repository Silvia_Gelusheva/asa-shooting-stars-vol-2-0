import { SimpleJunkie } from '../Junkies/SimpleJunkie'

export default function InviteJury({
  allJunkies,
  toggleInviteJuryMode,
  guestJuryChecked,
  openGuestJuryList,
  inviteJuryBtnClass,
  juryModalClass,
  closeGuestJuryList,
  addToGuestJuryList,
  removeFromGuestJuryList
}) {
  return (
    <div className="">
      <div className="flex flex-col gap-5 my-6">
        <label className="flex gap-1" htmlFor="inviteJuryMode">
          <span className="text-md text-primary">Invite Jury</span>
          <input
            type="checkbox"
            onChange={() => toggleInviteJuryMode()}
            checked={guestJuryChecked}
            className="toggle border-secondary"
            id="inviteJuryMode"
          />
        </label>
        <button onClick={() => openGuestJuryList()} className={inviteJuryBtnClass}>
          Invite List
        </button>
      </div>
      <div className={juryModalClass}>
        <div className="modal-box h-full">
          <p className="font-bold text-center mb-3 text-accent bg-slate-200 text-lg rounded-md py-2">
            INVITE GUEST JURY
          </p>
          <div className="modal-action flex justify-evenly pb-[10px] ">
            <button onClick={() => closeGuestJuryList()} className="btn btn-accent">
              Save
            </button>
            <button onClick={() => closeGuestJuryList()} className="btn btn-accent">
              Close
            </button>
          </div>
          <div className="flex flex-col justify-center items-center gap-5">
            {allJunkies
              ?.filter((el) => el.points >= 200)
              ?.map((junkie, index) => (
                <label key={index} className="flex justify-evenly items-center gap-5">
                  <SimpleJunkie user={junkie} redirect={false} id={index} key={index} />
                  <input
                    type="checkbox"
                    value={index}
                    onChange={(e) =>
                      e.target.checked
                        ? addToGuestJuryList(junkie.username)
                        : removeFromGuestJuryList(junkie.username)
                    }
                    className="checkbox checkbox-accent"
                    id={index}
                  />
                </label>
              ))}
          </div>
        </div>
      </div>
    </div>
  )
}
