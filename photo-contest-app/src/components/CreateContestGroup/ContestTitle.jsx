import './styles.css'
export default function ContestTitle({ title, setTitle, category, setCategory }) {
  return (
    <div className="flex flex-col justify-center items-center">
      {/* title */}

      <input
        id="title"
        type="text"
        placeholder="Title"
        className="input input-md my-[10px] bg-inherit border-2 border-primary text-primary w-full"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />

      {/* category */}
      <input
        id="category"
        type="text"
        placeholder="Category"
        className="input input-md my-[10px] bg-inherit border-2 border-primary text-primary w-full"
        value={category}
        onChange={(e) => setCategory(e.target.value)}
      />
    </div>
  )
}
