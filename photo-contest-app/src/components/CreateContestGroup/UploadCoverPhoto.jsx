export default function UploadCoverPhoto({ setCover, coverUrl, setCoverUrl }) {
  return (
    <div>
      <div className="flex items-center justify-center h-screen border border-primary rounded-md max-h-44 w-5/5 mb-2">
        <img src={coverUrl} alt="" className="has-mask h-full w-full object-cover" />
      </div>

      <input
        type="file"
        accept=".jpg, .jpeg .png"
        name="file"
        className="file-input file-input-primary border-2 border-primary bg-inherit file-input-additional w-full max-w-xs text-secondary"
         onChange={(e) => {
          setCover(e.target.files[0])
          setCoverUrl(URL.createObjectURL(e.target.files[0]))
        }}
      />
    </div>
  )
}
