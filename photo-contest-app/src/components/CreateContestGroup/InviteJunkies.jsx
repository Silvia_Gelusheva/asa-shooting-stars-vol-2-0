import { SimpleJunkie } from '../Junkies/SimpleJunkie'

export default function InviteJunkies({
  allJunkies,
  toggleInvitationalMode,
  invitationalChecked,
  openInviteJunkiesList,
  inviteListBtnClass,
  junkiesModalClass,
  closeInviteJunkiesList,
  addToInviteList,
  removeFromInviteList,
}) {
  return (
    <div className="">
      <div className="flex flex-col gap-5 my-6">
        <label className="flex gap-1" htmlFor="invitationalMode">
          <span className="text-md text-primary">Invite Junkie</span>
          <input
            type="checkbox"
            onChange={() => toggleInvitationalMode()}
            checked={invitationalChecked}
            className="toggle border border-secondary"
            id="invitatitonalMode"
          />
        </label>
        <button onClick={() => openInviteJunkiesList()} className={inviteListBtnClass}>
          Invite List
        </button>
      </div>
      <div className={junkiesModalClass}>
        <div className="modal-box h-[40rem]">
          <p className="font-bold text-center mb-3 text-secondary bg-slate-200 text-lg rounded-md py-2">
            INVITE USERS
          </p>
          <div className="modal-accent flex justify-evenly pb-[10px] ">
            <button onClick={() => closeInviteJunkiesList()} className="btn btn-accent">
              Save
            </button>
            <button onClick={() => closeInviteJunkiesList()} className="btn btn-accent">
              Close
            </button>
          </div>
          <div className="flex flex-col justify-center items-center gap-5">
            {allJunkies?.map((junkie, index) => (
              <label key={index} className="flex justify-evenly items-center gap-5">
                <SimpleJunkie user={junkie} redirect={false} id={index} key={index} />
                <input
                  type="checkbox"
                  value={index}
                  onChange={(e) =>
                    e.target.checked
                      ? addToInviteList(junkie.username)
                      : removeFromInviteList(junkie.username)
                  }
                  className="checkbox checkbox-accent"
                  id={index}
                />
              </label>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
