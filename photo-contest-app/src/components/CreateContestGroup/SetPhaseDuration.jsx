export default function SetPhaseDuration({
  phase1Duration,
  setPhase1Duration,
  phase2Duration,
  setPhase2Duration,
}) {
  return (
    <div className="flex flex-row justify-start items-start flex-wrap w-full">

      {/* phase 1 */}
      <div className="w-full">

        <label htmlFor="phase1Duration" className="label">
          <span className="text-md text-primary">
            Open: <span className="text-md text-primary">{phase1Duration}</span> days
          </span>
        </label>

        <div className="w-[100%]">
          <input
            id="phase1Duration"
            type="range"
            min="1"
            max="30"
            className="range range-primary bg-primary"
            value={phase1Duration}
            onChange={(e) => setPhase1Duration(e.target.value)}
          />
        </div>
      </div>

      {/* phase 2 */}
      <div className=" w-full">
        
        <label htmlFor="phase2Duration" className="label">
          <span className="text-md text-primary">
            Review: <span className="text-md text-primary"> {phase2Duration} </span> hours
          </span>
        </label>

        <div className="flex flex-row">
          <input
            id="phase2Duration"
            type="range"
            min="1"
            max="24"
            value={phase2Duration}
            onChange={(e) => setPhase2Duration(e.target.value)}
            className="range range-primary bg-primary"
          />
        </div>
      </div>
    </div>
  )
}


