import { CgProfile } from 'react-icons/cg'
export default function UploadAvatar({ currentUser, userData, uploadPicture }) {
  return (
    <div>
      {userData?.firstname === currentUser?.firstname ? (
        <form onSubmit={uploadPicture} className="flex flex-row gap-4">
          {/* <input type="file" name="file" accept=".jpeg, .jpg, .png" /> */}
          <input
            type="file"
            name="file"
            accept=".jpeg, .jpg"
            className="file-input file-input-bordered file-input-sm w-full max-w-xs"
          />
          <button
            type="submit"
            className="bg-primary uppercase text-white font-bold hover:shadow-md
                        shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2"
            style={{ transition: 'all .15s ease' }}>
            {' '}
            <CgProfile size={16} />
          </button>z
        </form>
      ) : null}
    </div>
  )
}
