import { BsFillArrowLeftCircleFill, BsFillArrowRightCircleFill } from 'react-icons/bs'

import { useState } from 'react'

export default function Slides({ slides }) {
  const [currentIndex, setCurrentIndex] = useState(0)

  const containerStyles = {
    maxWidth: '700px',
    width: '100%',
    height: '380px',
    margin: '0 auto',
    padding: '2px',
  }

  const sliderStyles = {
    height: '100%',
    position: 'relative',
  }

  const slideStyles = {
    width: '100%',
    height: '100%',
    borderRadius: '10px',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundImage: `url(${slides[currentIndex]?.photo})`,
  }

  const leftArrowStyles = {
    position: 'absolute',
    top: '50%',
    transform: 'translate(0, -50%)',
    left: '32px',
    fontSize: '45px',
    color: '#fff',
    zIndex: 1,
    cursor: 'pointer',
  }

  const rightArrowStyles = {
    position: 'absolute',
    top: '50%',
    transform: 'translate(0, -50%)',
    right: '32px',
    fontSize: '45px',
    color: '#fff',
    zIndex: 1,
    cursor: 'pointer',
  }

  const dotsContainerStyles = {
    display: 'flex',
    justifyContent: 'center',
    margin: '15px',
  }

  const dotStyles = {
    margin: '0 3px',
    cursor: 'pointer',
    fontSize: '10px',
    color: '#C8B194',
  }

  const goToPrevious = () => {
    const isFirstSlide = currentIndex === 0
    const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1
    setCurrentIndex(newIndex)
  }

  const goToNext = () => {
    const isLastSlide = currentIndex === slides.length - 1
    const newIndex = isLastSlide ? 0 : currentIndex + 1
    setCurrentIndex(newIndex)
  }

  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex)
  }

  return (
    <div style={containerStyles}>
      <div style={sliderStyles}>
        <div style={leftArrowStyles} onClick={goToPrevious}>
          <BsFillArrowLeftCircleFill />
        </div>
        <div style={rightArrowStyles} onClick={goToNext}>
          <BsFillArrowRightCircleFill />
        </div>
        <div style={slideStyles}></div>
        <div style={dotsContainerStyles}>
          {slides.map((slide, slideIndex) => (
            <div key={slideIndex} style={dotStyles} onClick={() => goToSlide(slideIndex)}></div>
          ))}
        </div>
      </div>
    </div>
  )
}
