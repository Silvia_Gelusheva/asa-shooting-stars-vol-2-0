export default function UserInfo({ currentUser, checkRankByPoints }) {
  return (
    <div>
      <div className="text-center mt-2">
        <h2 className="font-semibold text-gray-500">
          {' '}
          {currentUser?.firstname} {currentUser?.lastname}
        </h2>
        <h2 className="font-bold uppercase"> {currentUser?.username}</h2>

        <p className="text-gray-500 text-sm">{`Member since: ${new Date(
          currentUser?.registeredOn
        ).toLocaleDateString('en-GB')}`}</p>
      </div>
      <ul className="py-4 mt-2 text-gray-700 flex items-center justify-around">
        <li className="flex flex-col items-center justify-around">
          Rank
          <div className="font-bold text-black">
            {' '}
            {currentUser?.role === 1 ? checkRankByPoints(currentUser?.points) : "-"}
          </div>
        </li>
        <li className="flex flex-col items-center justify-between">
          Points
          <div className="font-bold text-black">
          {currentUser?.role === 1 ? currentUser?.points : "-"}
             </div>
        </li>
        <li className="flex flex-col items-center justify-around">
          Contests
          <div className="font-bold text-black">
            {' '}
            {currentUser?.role === 1 ? Object.keys(currentUser?.participations || []).length : "-"}
          </div>
        </li>
      </ul>
    </div>
  )
}
