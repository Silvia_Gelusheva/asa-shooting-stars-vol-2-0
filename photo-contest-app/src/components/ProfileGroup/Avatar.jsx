import defaultAvatar from '../../assets/empty.jpg'

export default function Avatar({ currentUser }) {
  return (
    <div>
      <div className="rounded-t-lg h-32 overflow-hidden">
        <img
          className="object-cover object-top w-full opacity-30"
          src="https://t4.ftcdn.net/jpg/04/17/79/11/360_F_417791183_ZD9GOJtMTk0Z11JEjaBeCyUuMM9iwx0L.jpg"
          alt="Mountain"
        />
      </div>
      <div className="mx-auto w-32 h-32 relative -mt-16 border-4 border-white rounded-full overflow-hidden">
        {currentUser?.avatarUrl ? (
          <img
            className="object-cover object-center h-32"
            src={currentUser?.avatarUrl}
            alt="avatar"
          />
        ) : (
          <img className="object-cover object-center h-32" src={defaultAvatar} alt="profile" />
        )}
      </div>     
    </div>
  )
}
