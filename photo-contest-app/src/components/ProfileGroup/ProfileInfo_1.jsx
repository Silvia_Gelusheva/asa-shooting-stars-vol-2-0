import Avatar from './Avatar'
import { CgProfile } from 'react-icons/cg'
import defaultAvatar from '../../assets/empty.jpg'
export default function ProfileInfo_1({ currentUser, uploadPicture, userData }) {
  return (
    <div className="">
      <div className="mx-auto w-32 h-32 border-4 border-primary rounded-full overflow-hidden">
        {currentUser?.avatarUrl ? (
          <img
            className="object-cover object-center w-32 h-32"
            src={currentUser?.avatarUrl}
            alt="avatar"
          />
        ) : (
          <img className="object-cover object-center h-32" src={defaultAvatar} alt="profile" />
        )}
      </div>

      <p className="text-3xl">{currentUser?.username}</p>
      <p className="text-sm">
        {currentUser?.firstname} {currentUser?.lastname}
      </p>
      <p className="text-gray-500 text-sm ">
        {`Member since: ${new Date(currentUser?.registeredOn).toLocaleDateString('en-GB')}`}
      </p>
      {userData?.firstname === currentUser?.firstname ? (
        <form
          onSubmit={uploadPicture}
          className="flex flex-col justify-center items-center gap-1 mt-6">
          <input
            type="file"
            name="file"
           
            className="file-input file-input-primary file-input-sm w-full max-w-xs"
          />
          <button type="submit" className="btn btn-primary btn-circle text-secondary">
            <CgProfile size={20} />
          </button>
        </form>
      ) : null}
    </div>
  )
}
