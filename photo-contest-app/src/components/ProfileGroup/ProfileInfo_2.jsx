import { CgProfile } from 'react-icons/cg'
import defaultAvatar from '../../assets/empty.jpg'
export default function ProfileInfo_2({ currentUser, checkRankByPoints, uploadPicture, userData }) {
  return (
    <div className="flex flex-row items-center justify-center gap-6 text-lg text-primary mb-8">
      <div className="flex flex-col items-center justify-center">
        <p > Rank</p>
        <p className='font-semibold text-xl'>{currentUser?.role === 1 ? checkRankByPoints(currentUser?.points) : '-'}</p>
      </div>
      <div className="flex flex-col items-center justify-center">
        <p>Points</p>
        <p className='font-semibold text-xl'>{currentUser?.role === 1 ? currentUser?.points : '-'}</p>
      </div>
      <div className="flex flex-col items-center justify-center">
        <p> Contests</p>
        <p className='font-semibold text-xl'>
          {currentUser?.role === 1 ? Object.keys(currentUser?.participations || []).length : '-'}
        </p>
      </div>
    </div>
  )
}
