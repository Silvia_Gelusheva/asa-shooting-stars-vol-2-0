import Avatar from '../../components/ProfileGroup/Avatar'
// import PhotoSlider from '../../components/PhotoSlider/PhotoSlider'
// import SinglePhotoSlider from '../../components/SinglePhotoSlider/SinglePhotoSlider'
import Slides from '../../components/ProfileGroup/Slides'
import UploadAvatar from '../../components/ProfileGroup/UploadAvatar'
import UserInfo from '../../components/ProfileGroup/UserInfo'

export default function ProfileCard({currentUser, checkRankByPoints, uploadPicture, userData}) {
     return (
  
      <div className="max-w-2xl mx-4 sm:max-w-sm md:max-w-sm lg:max-w-sm xl:max-w-sm sm:mx-auto md:mx-auto xl:mx-auto mt-16 bg-white shadow-xl rounded-lg text-gray-900">
        <Avatar currentUser={currentUser} />
        <UserInfo currentUser={currentUser} checkRankByPoints={checkRankByPoints} />
        <div className="py-4 border-t mx-8 mt-2">
          <UploadAvatar
            currentUser={currentUser}
            userData={userData}
            uploadPicture={uploadPicture}
          />
        </div>
      </div>    

  )
}
