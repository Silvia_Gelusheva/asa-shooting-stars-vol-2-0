import { deleteContest, getAllContests } from '../../services/contest.services'
import { useEffect, useMemo, useRef, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

import { HiArrowDown } from 'react-icons/hi'
import { HiArrowUp } from 'react-icons/hi'
import PaginationContestsTable from './PaginationContestsTable'
import styled from 'daisyui/dist/styled'

export default function ContestTable() {
  const [contests, setContests] = useState([])
  const [sorted, setSorted] = useState({ sorted: 'title', reversed: false })
  const [query, setQuery] = useState('')
  const navigate = useNavigate()
  const { username } = useParams()
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(5)
  const lastPostIndex = currentPage * postsPerPage
  const firstPostIndex = lastPostIndex - postsPerPage
  const currentPosts = contests?.slice(firstPostIndex, lastPostIndex)
  useEffect(() => {
    getAllContests()
      .then((allContests) => {
        setContests(allContests)
      })
      .catch((e) => console.log(e.message))
  }, [])

  const sortByTitle = () => {
    setSorted({ sorted: 'title', reversed: !sorted.reversed })

    const contestsCopy = [...contests]
    contestsCopy.sort((itemA, itemB) => {
      if (sorted.reversed) {
        return itemA.title.localeCompare(itemB.title)
      }

      return itemB.title.localeCompare(itemA.title)
    })

    setContests(contestsCopy)
  }
  const sortByCategory = () => {
    setSorted({ sorted: 'category', reversed: !sorted.reversed })

    const contestsCopy = [...contests]
    contestsCopy.sort((itemA, itemB) => {
      if (sorted.reversed) {
        return itemA.title.localeCompare(itemB.title)
      }

      return itemB.title.localeCompare(itemA.title)
    })

    setContests(contestsCopy)
  }
  const filteredContests = useMemo(
    () =>
      contests?.filter((contests) => {
        return (
          contests?.title.toLowerCase().includes(query.toLowerCase()) ||
          contests?.category.toLowerCase().includes(query.toLowerCase())
        )
      }),
    [contests, query]
  )
  const styledRow =
    'flex flex-col lg:flex-row w-full whitespace-pre-line lg:w-auto px-10 py-1 lg:table-cell cursor-pointer relative lg:static'
  const renderRow = () => {
    return filteredContests?.slice(firstPostIndex, lastPostIndex).map((contest) => {
      return (
        <tr
          key={contest?.title}
          className="bg-inherit flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-8 lg:mb-0 lg:w-full">
          <td className={styledRow}>
            {' '}
            <button onClick={() => navigate(`contest${contest?.contestId}`)}>
              {contest?.title}
            </button>
          </td>
          <td className={styledRow}>{contest?.category}</td>
          <td className={styledRow}>{new Date(contest?.createdOn).toLocaleDateString()}</td>
          <td className={styledRow}>{new Date(contest?.phaseTwoEndTime).toLocaleDateString()}</td>
          <td className={styledRow}>
            <button
              className="btn btn-sm bg-gray-800 text-gray-200"
              onClick={() => removeContest(contest?.contestId, username)}>
              DELETE
            </button>
          </td>
        </tr>
      )
    })
  }

  const renderArrow = () => {
    if (sorted.reversed) {
      return <HiArrowUp size={16} />
    }
    return <HiArrowDown size={16} />
  }

  const removeContest = (contestId, username) => {
    deleteContest(contestId, username).then(() => {
      // setContests((state) => ({
      //   ...state,
      //   contests: contests.filter((c) => c.id !== contestId),
      // }))
      setContests(contests.filter((c) => c.id !== contestId))
      navigate('/dashboard')
    })

    // window.location.reload(false);
  }

  return (
    <div className="sort flex flex-col items-center">
      <div className="flex my-8 ">
        <input
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          type="search"
          className=" input border border-solid border-black rounded-md "
          placeholder="search..."
        />
      </div>

      <table className="w-full table opacity-90 flex">
        <thead>
          <tr>
            <th
              onClick={sortByTitle}
              className="p-3 font-bold bg-info text-lg text-center uppercase hidden lg:table-cell cursor-pointer">
              <span className="flex flex-row">
                title
                {sorted.sorted === 'title' ? renderArrow() : null}
              </span>
            </th>
            <th
              onClick={sortByCategory}
              className="p-3 font-bold bg-info text-lg text-center uppercase hidden lg:table-cell cursor-pointer">
              <span className="flex flex-row">
                category
                {sorted.sorted ===
                'p-3 font-bold bg-info text-lg text-center uppercase hidden lg:table-cell category'
                  ? renderArrow()
                  : null}
              </span>
            </th>
            <th className="p-3 font-bold bg-info text-lg text-center uppercase hidden lg:table-cell cursor-pointer">
              <span>Start</span>
            </th>
            <th className="p-3 font-bold bg-info text-lg text-center uppercase hidden lg:table-cell cursor-pointer">
              <span className="flex flex-row">end</span>
            </th>
            <th className="p-3 font-bold bg-info text-lg text-center uppercase hidden lg:table-cell cursor-pointer">
              <span className="flex flex-row">action</span>
            </th>
          </tr>
        </thead>

        <tbody>{renderRow()}</tbody>
      </table>
      <PaginationContestsTable
        totalPosts={contests?.length}
        postsPerPage={postsPerPage}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </div>
  )
}
