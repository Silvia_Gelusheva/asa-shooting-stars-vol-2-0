import { useEffect, useMemo, useState } from 'react'

import { HiArrowDown } from 'react-icons/hi'
import { HiArrowUp } from 'react-icons/hi'
import PaginationContestsTable from './PaginationContestsTable'
import { getAllUsers } from '../../services/user.service'
import { useNavigate } from "react-router-dom"

export default function Junkies() {
 
  const navigate = useNavigate()
  const [sorted, setSorted] = useState({ sorted: 'firstname', reversed: false })
  const [query, setQuery] = useState('')
  const [users, setUsers] = useState([])

  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(5)
  const lastPostIndex = currentPage * postsPerPage
  const firstPostIndex = lastPostIndex - postsPerPage

  useEffect(() => {
    getAllUsers()
      .then(setUsers)
      .then(console.log(users))
      .catch((e) => console.log(e.message))
  }, [])

  const sortByFirstname = () => {
    setUsers({ sorted: 'firstname', reversed: !sorted.reversed })

    const usersCopy = [...users]
    usersCopy.sort((itemA, itemB) => {
      if (sorted.reversed) {
        return itemA.title.localeCompare(itemB.title)
      }

      return itemB.title.localeCompare(itemA.title)
    })

    setUsers(usersCopy)
  }

  const sortByUsername = () => {
    setUsers({ sorted: 'username', reversed: !sorted.reversed })
    const usersCopy = [...users]
    usersCopy.sort((itemA, itemB) => {
      if (sorted.reversed) {
        return itemA.title.localeCompare(itemB.title)
      }

      return itemB.title.localeCompare(itemA.title)
    })

    setUsers(usersCopy)
  }

  const filteredUsers = useMemo(
    () =>
      users.filter((users) => {
        return users.firstname.toLowerCase().includes(query.toLowerCase())
      }),
    [users, query]
  )

  const renderRow = () => {
    return filteredUsers.slice(firstPostIndex, lastPostIndex).map((user) => {
      return (
        <tr key={user?.username}>
          <td> {user?.username} </td>
          <td>{user?.firstname}</td>
          <td>{user?.lastname}</td>
          <td>{new Date(user?.registeredOn).toLocaleDateString()}</td>
          <td>
            <button onClick={() => navigate(`/profile/${user?.username}`)} className="btn btn-sm bg-gray-800 text-gray-200">PROFILE</button>
          </td>
        </tr>
      )
    })
  }

  const renderArrow = () => {
    if (sorted.reversed) {
      return <HiArrowUp size={16} />
    }
    return <HiArrowDown size={16} />
  }

  return (
    <div className="sort flex flex-col items-center">
      <div className="flex my-8 ">
        <input
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          type="search"
          className=" input border border-solid border-black rounded-md "
          placeholder="search..."
        />
      </div>

      <table className="table w-full shadow-lg">
        <thead>
          <tr>
            <th className="cursor-pointer" onClick={sortByFirstname}>
              <span className="flex flex-row">
                username
                {sorted.sorted === 'title' ? renderArrow() : null}
              </span>
            </th>
            <th className="cursor-pointer" onClick={sortByUsername}>
              <span className="flex flex-row">
                name
                {sorted.sorted === 'title' ? renderArrow() : null}
              </span>
            </th>
            <th className="cursor-pointer">
              <span>lastname</span>
            </th>
            <th className="cursor-pointer">
              <span>Member since</span>
            </th>
            <th className="cursor-pointer">
              <span>Visit</span>
            </th>
          </tr>
        </thead>

        <tbody>{renderRow()}</tbody>
      </table>
      <PaginationContestsTable
        totalPosts={users.length}
        postsPerPage={postsPerPage}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </div>
  )
}
