import moment from "moment";

export const COLUMNS_MEMBERS = [
  {
    Header: "USERNAME",
    accessor: "username",
  },
  {
    Header: "Name",
    accessor: "firstname",
  },
  {
    Header: "Lastname",
    accessor: "lastname",
  }, 
  {
    Header: "Joined On",
    accessor: "registeredOn",
    Cell: (props) => moment(new Date(props.value)).format("L"),
  },
  {
    Header: "points",
    accessor: "points",
  },   
];