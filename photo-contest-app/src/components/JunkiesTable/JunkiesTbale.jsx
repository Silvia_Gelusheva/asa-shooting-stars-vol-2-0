import { useEffect, useMemo, useState } from 'react'

import { AppContext } from '../../context/app.context'
import { CgSearchLoading } from 'react-icons/cg'
import defaultAvatar from '../../assets/empty.jpg'
import { getAllUsers } from '../../services/user.service'
import { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

export const JunkiesTable = () => {
  const [users, setUsers] = useState([])
  const [query, setQuery] = useState('')
  const { userData } = useContext(AppContext)
  const navigate = useNavigate()

  useEffect(() => {
    getAllUsers()
      .then(setUsers)
      .then(console.log(users))
      .catch((e) => console.log(e.message))
  }, [])

  const filteredUsers = useMemo(
    () =>
      users.filter((users) => {
        return users.firstname.toLowerCase().includes(query.toLowerCase())
      }),
    [users, query]
  )

  return (
    <div>
      {userData?.isActive === false && navigate('/blocked')}
      <div className="flex flex-col justify-center items-center gap-8">
        <p className="underline text-3xl opacity-75 mt-6">ASA Shooting Stars Community</p>
        {/* search bar */}
        <div className="relative text-primary">
          <input
            type="search"
            placeholder="Username..."
            value={query}
            onChange={(e) => setQuery(e.target.value)}
            className="border-primary border-2 h-10 px-5 pr-10 rounded-full text-sm focus:outline-none"
          />
          <button type="submit" className="absolute right-0 top-0 mt-3 mr-4 text-primary">
            <CgSearchLoading />
          </button>
        </div>

        {/* avatars */}
        <div className="grid lg:grid-cols-10 md:grid-cols-4 grid-cols-3 gap-8">
          {filteredUsers.map((user) => (
            <div
              key={user?.username}
              className="flex flex-col justify-center items-center cursor-pointer text-secondary hover:text-primary"
              onClick={() => navigate(`/profile/${user?.username}`)}>
              <img
                className="object-cover object-center h-20 w-20 rounded-full"
                src={user?.avatarUrl || defaultAvatar}
                alt="avatar"
              />
              <p>{user?.username}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
