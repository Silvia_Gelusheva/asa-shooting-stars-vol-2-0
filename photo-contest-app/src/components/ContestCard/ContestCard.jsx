import './ContestCard.scss'

/* eslint-disable react/prop-types */
import { useNavigate, useParams } from 'react-router-dom'

export default function ContestCard({ contest }) {
  const navigate = useNavigate()
  const { phase } = useParams()

  return (
    <div className="">
      <div className="card card-1">
        <div className="card-img"></div>
        <a href="" className="card-link">
          <div
            className="card-img-hovered"
            onClick={() => navigate(`contest${contest.contestId}`)}
            style={{ backgroundImage: `url(${contest.cover})`, backgroundSize: 'cover' }}></div>
        </a>
        <div className="card-info">
          <div className="card-about text-sm text-gray-600 font-serif">
            <p>Start: {new Date(contest.createdOn).toLocaleDateString()}</p>
            <p>End: {new Date(contest.phaseTwoEndTime).toLocaleDateString()}</p>
          </div>
          <h1 className="card-title text-sm text-gray-700 font-bold font-serif">{contest.title}</h1>
          <div className="card-category text-start text-sm text-gray-600 font-serif">{contest.category}</div>
        </div>
      </div>
    </div>
  )
}


