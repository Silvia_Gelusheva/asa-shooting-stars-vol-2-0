import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry'

import { getAllParticipationsByContestId } from '../../services/participation.services'
import { getParticipationById } from '../../services/contest.services'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'

export default function PhotoGrid({ contestId, finished = false }) {
  const navigate = useNavigate()
  const [participationIds, setParticipationIds] = useState([])
  const [participations, setParticipations] = useState([])
  useEffect(() => {
    getAllParticipationsByContestId(contestId)
      .then(setParticipationIds)
      .catch((e) => console.log(e.message))
  }, [contestId])

  useEffect(() => {
    Promise.all(participationIds?.map((part) => getParticipationById(part)))
      .then((result) =>
        finished
          ? setParticipations(result.sort((a, b) => b.score - a.score))
          : setParticipations(result)
      )
      .catch((e) => console.log(e.message))
  }, [participationIds])

  return (
    <>
      <div className="">
        <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 4}}>
          <Masonry gutter="20px">
            {participations.map((item, index) => (
              <div key={'container' + index}>
                {index < 3 && finished && (
                  <div className="text-right mr-[10px] mb-[-30px]">
                    <span
                      key={`indicator${index}`}
                      className="badge badge-lg bg-[#a67c00] relative text-xl text-white">
                      {' '}
                      {index + 1} 
                    </span>
                  </div>
                )}
                <img
                  key={'pik' + item?.id}
                  src={item?.photo}
                  className="w-full block cursor-pointer border-4 rounded border-primary"
                  onClick={() => navigate(`participation${item?.id}`)}
                />
              </div>
            ))}
          </Masonry>
        </ResponsiveMasonry>
      </div>
    </>
  )
}


