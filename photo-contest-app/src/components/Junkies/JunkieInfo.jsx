export const JunkieInfo = () => {
  return (
    <div className="flex justify-center">
      <div className="flex items-center flex-wrap whitespace-pre-wrap gap-5 px-10 py-2 max-w-[600px] justify-center w-full rounded-md bg-secondary">
        <div className="flex flex-col justify-center items-center">
          <div className="avatar">
            <div className="w-24 rounded">
              <img src="https://placeimg.com/192/192/people" />
            </div>
          </div>
          <p>Username</p>
        </div>
        <div className="flex flex-col justify-center items-start">
          <p>score: 148</p>
          <p>Rank: Enthusiast</p>
          <p>Points for Rank up: 12</p>
        </div>
        <button className="btn btn-primary">go to profile ?</button>
      </div>
    </div>
  )
}
