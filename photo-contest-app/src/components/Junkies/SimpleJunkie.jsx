import { AppContext } from '../../context/app.context'
import { checkRankByPoints } from '../../utils/helpers'
import defaultAvatar from '../../assets/empty.jpg'
import { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

/* eslint-disable react/prop-types */
export const SimpleJunkie = ({ user, id, redirect = false }) => {
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)
  const navigate = useNavigate()

  return (
    <div
      onClick={() => redirect && navigate(`/profile/${user?.username}`)}
      className=" w-[300px] min-w-[300px] border-2 border-accent py-2 indicator rounded-xl hover:scale-[1.1] transition-all
      flex gap-2 items-start justify-around cursor-pointer bg-base-100 text-accent text-sm">
      {id < 3 && (
        <span className="indicator-item badge badge-black bg-[#a67c00] text-md text-white">
          {' '}
          top {id + 1}
        </span>
      )}
      <div className="avatar">
        <div className="w-12 rounded-full">
          <img src={user?.avatarUrl ? user?.avatarUrl : defaultAvatar} />
        </div>
      </div>
      <div>
        {/* eslint-disable-next-line react/prop-types */}
        <p className="text-black text-lg">
          {user.firstname} {user.lastname}
        </p>
        <p className="text-sm text-black">{checkRankByPoints(user.points)}</p>
      </div>
      <div>
        <p className="text-black text-md">Points</p>
        {/* eslint-disable-next-line react/prop-types */}
        <p className="text-black text-md">{user.points}</p>
      </div>
    </div>
  )
}
