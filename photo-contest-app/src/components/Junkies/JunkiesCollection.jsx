import { useEffect, useState } from 'react'

import { SimpleJunkie } from './SimpleJunkie'
import { getAllUsers } from '../../services/user.service'

export const JunkiesCollection = () => {
  const [allUsers, setAllUsers] = useState([])

  useEffect(() => {
    getAllUsers()
      .then(setAllUsers)
      .catch((e) => {
        console.log(e.message)
      })
  }, [])
  const allJunkies = allUsers?.filter((user) => user.role === 1).sort((a, b) => b.points - a.points)

  const [page, setPage] = useState(1)
  const [prevDisable, setPrevDisable] = useState(true)
  const [nextDisable, setNextDisable] = useState(false)

  useEffect(() => {
    if (page === 1) {
      setPrevDisable(true)
    } else {
      setPrevDisable(false)
    }
    if (page >= allJunkies.length / 10) {
      setNextDisable(true)
    } else {
      setNextDisable(false)
    }
  }, [page])

  return (
    <div className="p-10 flex flex-col justify-center items-center">
      <div>
        <div className="flex justify-center items-center w-full">
          <hr className="my-8 w-64 h-px bg-warning border-0" />
          <span className="absolute left-1/2 px-3 font-medium text-gray-900 bg-base-100 -translate-x-1/2 dark:text-white dark:bg-gray-900">
            {' '}
            <h3 className="text-xl text-[#bf9b30]"> All Stars </h3>
          </span>
        </div>
      </div>

      <div className="w-full p-10 flex justify-evenly gap-3 flex-wrap">
        {allJunkies.map((el, index) =>
          index >= (page - 1) * 10 && index <= page * 10 - 1 ? (
            <SimpleJunkie user={el} id={index} key={index} redirect={true} />
          ) : null
        )}
      </div>
      <div className="btn-group">
        <button disabled={prevDisable} onClick={() => setPage(page - 1)} className="btn  w-[50px]">
          «
        </button>
        <button className="btn btn-white">{page}</button>
        <button disabled={nextDisable} onClick={() => setPage(page + 1)} className="btn w-[50px]">
          »
        </button>
      </div>
    </div>
  )
}
