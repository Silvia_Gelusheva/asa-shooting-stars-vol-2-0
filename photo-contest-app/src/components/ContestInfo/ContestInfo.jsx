import { useEffect, useState } from 'react'

import CountdownTimer from '../CountdownTimer/CountdownTimer'

export const ContestInfo = ({ contest, phase }) => {
  const [endTime, setEndTime] = useState(0)
  useEffect(() => {
    if (phase === 'registration') {
      setEndTime(contest?.phaseOneEndTime)
    } else if (phase === 'review') {
      setEndTime(contest?.phaseTwoEndTime)
    }
  }, [contest])

  return (
    <div className="flex flex-col w-full lg:flex-row my-6">
      <div className="flex flex-col gap-1 card-container h-32 bg-inherit place-items-center font-bold">
        {' '}
        <p className="text-center text-4xl text-gray-700">Category: {contest?.category}</p>
        <p className="text-center text-3xl text-accent">Contest: {contest?.title}</p>
      </div>
      <div className="divider lg:divider-horizontal"></div>
      <div className="card-container h-32 bg-inherit place-items-center">
        {phase === 'finished' ? (
          <p className="text-center text-2xl text-gray-700 font-bold">Expired</p>
        ) : (
          <div>
            <CountdownTimer endTimeMS={endTime} />
          </div>
        )}
      </div>
    </div>    
  )
}
