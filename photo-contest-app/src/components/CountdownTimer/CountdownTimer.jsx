import { useEffect, useState } from 'react'

import { getRemainingTimeUntilMSTimestamp } from '../../utils/countdown.utils'

const defaultRemainingTime = {
  seconds: '00',
  minutes: '00',
  hours: '00',
  days: '00',
}
export default function CountdownTimer({ endTimeMS }) {
  const [remainingTime, setRemainingTime] = useState(defaultRemainingTime)
  useEffect(() => {
    const interval = setInterval(() => {
      updateRemainingTime(endTimeMS)
    }, 1000)
    return () => clearInterval(interval)
  }, [endTimeMS])

  function updateRemainingTime(endTimeMS) {
    setRemainingTime(getRemainingTimeUntilMSTimestamp(endTimeMS))
  }
  return (
    <div className="flex justify-center">
      <div className="grid grid-flow-col gap-3 text-center auto-cols-max">
        <div className="flex flex-col p-2 bg-accent opacity-75 text-black rounded-box">
          <span className="countdown text-5xl">
            <span style={{ '--value': remainingTime.days }}></span>
          </span>
          days
        </div>
        <div className="flex flex-col p-2 bg-accent opacity-75 text-black rounded-box">
          <span className="countdown text-5xl">
            <span style={{ '--value': remainingTime.hours }}></span>
          </span>
          hours
        </div>
        <div className="flex flex-col p-2 bg-accent opacity-75 text-black rounded-box">
          <span className="countdown text-5xl">
            <span style={{ '--value': remainingTime.minutes }}></span>
          </span>
          min
        </div>
        <div className="flex flex-col p-2 bg-black opacity-75 text-accent rounded-box">
          <span className="countdown text-5xl">
            <span style={{ '--value': remainingTime.seconds }}></span>
          </span>
          sec
        </div>
      </div>
    </div>
  )
}
