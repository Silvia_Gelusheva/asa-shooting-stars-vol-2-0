export default function Upload({ form, photoUrl, validateForm, setForm, setPhotoUrl }) {
  return (
    <div>
      <div className="flex justify-center items-center w-full">
        <img src={photoUrl} />
      </div>
      <div className="flex flex-col flex-wrap justify-center">
       <input
          type="file"
          className="file-input file-input-accent file-input-bordered text-secondary border-y-4 border-x-2 border-[#E6C8A0] w-full mt-2"
          accept=".jpg, .jpeg, .png"
          onChange={(e) => {
            setForm({ ...form, photoUrl: e.target.files[0] })
            setPhotoUrl(URL.createObjectURL(e.target.files[0]))
          }}
        />
        <button
          onClick={() => validateForm(form)}
          type="button"
          className=" bg-primary text-accent hover:bg-accent hover:text-primary justify-center my-5 p-3 font-bold italic font-serif shadow-lg cursor-pointer"
          >
          Submit
        </button>
      </div>
    </div>
  )
}
