import { Fragment, useContext, useRef, useState } from 'react'
import {
  PHOTO_STORY_MAX_LENGTH,
  PHOTO_STORY_MIN_LENGTH,
  PHOTO_TITLE_MAX_LENGTH,
  PHOTO_TITLE_MIN_LENGTH,
} from '../../common/constants'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { useNavigate, useParams } from 'react-router-dom'

import { AppContext } from '../../context/app.context'
import Confirmation from './Confirmation.jsx'
import PhotoTitle from './PhotoTitle.jsx'
import Upload from './Upload.jsx'
import { addContestParticipation } from '../../services/contest.services'
import { storage } from '../../firebase/config'
import { v4 } from 'uuid'

export default function PhotoUpload() {
  const [photoUrl, setPhotoUrl] = useState('')
  const { user, addToast, userData } = useContext(AppContext)
  const navigate = useNavigate()
  const { contestId } = useParams()
  const [open, setOpen] = useState(false)
  const cancelButtonRef = useRef(null)
  const [form, setForm] = useState({
    title: {
      value: '',
      touched: '',
      valid: false,
      error: '',
    },
    story: {
      value: '',
      touched: '',
      valid: false,
      error: '',
    },
    photoUrl: '',
  })

  const updateTitle = (value = '') => {
    setForm({
      ...form,
      title: {
        value,
        touched: true,
        valid:
          value.trim().length >= PHOTO_TITLE_MIN_LENGTH &&
          value.trim().length <= PHOTO_TITLE_MAX_LENGTH,
        error:
          value.length < PHOTO_TITLE_MIN_LENGTH
            ? `Minimum title length: ${PHOTO_TITLE_MIN_LENGTH}`
            : `Maximum title length: ${PHOTO_TITLE_MAX_LENGTH}`,
      },
    })
  }
  const updateStory = (value = '') => {
    setForm({
      ...form,
      story: {
        value,
        touched: true,
        valid:
          value.trim().length >= PHOTO_STORY_MIN_LENGTH &&
          value.trim().length <= PHOTO_STORY_MAX_LENGTH,
        error:
          value.length < PHOTO_STORY_MIN_LENGTH
            ? `Minimum story length: ${PHOTO_STORY_MIN_LENGTH}`
            : `Maximum story length: ${PHOTO_STORY_MAX_LENGTH}`,
      },
    })
  }
  const validateForm = (form) => {
    if (!form.photoUrl) return addToast('error', 'Please select a file!')
    if (!form.title.valid) return addToast('error', 'Invalid title!')
    if (!form.story.valid) return addToast('error', 'Invalid story!')
    setOpen(true)
  }

  const uploadPhoto = async (e) => {
    e.preventDefault()
    const file = form.photoUrl
    const photoRef = ref(storage, `images/contests/${v4()}`)
    try {
      const result = await uploadBytes(photoRef, file)
      const url = await getDownloadURL(result.ref)
      await addContestParticipation(
        form.title.value,
        form.story.value,
        url,
        userData.username,
        contestId
      )
      addToast('success', 'Photo has been uploaded successfully!')
      navigate('/dashboard')
    } catch (error) {
      addToast('error', error.message)
    }
  }

  return (
    <div className="min-h-full">
      <form id="participationForm" onSubmit={uploadPhoto}>
        <div className="flex flex-col bg-inherit w-[40vw] justify-center lg:pb-12">
          <PhotoTitle form={form} updateTitle={updateTitle} updateStory={updateStory} />
          <Upload
            form={form}
            photoUrl={photoUrl}
            validateForm={validateForm}
            setForm={setForm}
            setPhotoUrl={setPhotoUrl}
          />
        </div>
        <Confirmation
          open={open}
          Fragment={Fragment}
          cancelButtonRef={cancelButtonRef}
          setOpen={setOpen}
          uploadPhoto={uploadPhoto}
        />
      </form>
    </div>
  )
}
