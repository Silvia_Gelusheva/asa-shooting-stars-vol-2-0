export default function PhotoTitle({ form, updateTitle, updateStory }) {
  return (
    <div className="flex items-center flex-col">
      {/* title */}
      <input
        className="bg-neutral text-primary p-2 border-y-4 border-x-2 border-accent rounded-full focus:outline-none focus:border-accent w-full"
        type="text"
        id="title"
        placeholder="Title"
        value={form.title.value}
        onChange={(e) => updateTitle(e.target.value)}
      />
      <label className="label">
        {form.title.touched && !form.title.valid && (
          <span className="label-text-alt text-red-300">{form.title.error}</span>
        )}
      </label>

      {/* story */}
      <textarea
        className="bg-neutral text-primary p-2 h-60 border-y-4 border-x-2 border-accent rounded-xl focus:outline-none focus:border-accent w-full"
        type="textarea"
        id="story"
        placeholder="Story"
        value={form.story.value}
        onChange={(e) => updateStory(e.target.value)}
      />
      <label className="label">
        {form.story.touched && !form.story.valid && (
          <span className="label-text-alt text-red-300">{form.story.error}</span>
        )}
      </label>
    </div>
  )
}
