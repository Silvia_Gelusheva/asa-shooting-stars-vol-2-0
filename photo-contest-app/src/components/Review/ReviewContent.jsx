import { useEffect, useState } from 'react'

import { AppContext } from '../../context/app.context'
import { MdDateRange } from 'react-icons/md'
import StarRating from './StarRating'
import defaultAvatar from '../../assets/empty.jpg'
import { getAvatarByUsername } from '../../services/user.service'
import { getReviewById } from '../../services/review.services'
import { useContext } from 'react'

export default function ReviewContent({ review, isActive }) {
  const { addToast, userData, setAppState, user } = useContext(AppContext)
  const [avatar, setAvatar] = useState('')

  useEffect(() => {
    getAvatarByUsername(review?.jury)
      .then(setAvatar)
      .catch((error) => addToast('error', error.message))
  }, [])

  return (
    <div className=" flex w-full">
      <div className="w-full max-w-screen-sm m-auto p-3 min-h-100 overflow-hidden">
        <div className="wrapper bg-gray flex flex-row p-3 border-4 border-gray-100 rounded-3xl shadow-md">
          <div className="w-1/6 flex-grow-0">
            <div className="rounded-full w-full h-auto p-1 overflow-hidden">
              {/* border-green-700 border-4  */}
              <img
                className="rounded-full w-full"
                src={avatar ? avatar : defaultAvatar}
                alt="avatar"
              />
            </div>
          </div>
          <div className="w-5/6 info text-left pl-3 text-gray-600">
            <div className="written-by uppercase text-gray-600 tracking-wide text-sm">
              Review by {review?.jury}
            </div>
            <small>
              <span className="flex flex-row text-gray-500 justify-start">
                <StarRating score={review?.rating} />
              </span>
            </small>

            <div className="text-sm text-gray-600">
              {review?.content}
              <small>
                <span className="flex flex-row text-gray-500 justify-end">
                  <MdDateRange /> <p>{new Date(review?.createdOn).toLocaleString('en-GB')}</p>
                </span>
              </small>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
