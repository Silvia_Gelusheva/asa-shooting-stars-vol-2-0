import { AppContext } from '../../context/app.context'
import { GiFallingStar } from 'react-icons/gi'
import { addReview } from '../../services/review.services'
import { set } from 'firebase/database'
import { useContext } from 'react'
import { useParams } from 'react-router-dom'
import { useState } from 'react'

export default function CreateReview({ setReview }) {
  const { addToast, userData, setAppState, appState, user } = useContext(AppContext)
  const stars = Array(10).fill(0)
  const [rating, setRating] = useState(0)
  const [hover, setHover] = useState(null)
  const [comment, setComment] = useState('')
  const { participationId } = useParams()
  // TODO participation.author

  const handleSubmit = async (e) => {
    e.preventDefault()
    setReview({
      jury: userData.username,
      participationId,
      rating,
      content: comment,
      createdOn: Date.now(),
    })
    try {
      await addReview(userData.username, participationId, rating, comment)
    } catch (error) {
      addToast('error', error.message)
    }
  }

  return (
    <div className="flex justify-center items-center w-full overflow-hidden">
      <div className="flex justify-center items-center bg-gray border-4 border-white  rounded-3xl shadow-md ">
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className='px-40 py-4'>
            {/* starRating */}
            <small>
              <span className="flex flex-row text-gray-400 justify-around">
                <div className="flex flex-row">
                  {stars.map((_, index) => {
                    const ratingValue = index + 1
                    return (
                      <label>
                        <input
                          className="hidden"
                          type="radio"
                          name="rating"
                          value={ratingValue}
                          onClick={() => setRating(ratingValue)}
                        />
                        <GiFallingStar
                          className="cursor-pointer transition-shadow"
                          key={index}
                          size={24}
                          color={ratingValue <= (hover || rating) ? '#FFCC66' : '#BCBCBC'}
                          onMouseOver={() => setHover(index + 1)}
                          onMouseLeave={() => setHover(null)}
                        />
                      </label>
                    )
                  })}
                </div>
                <p className="text text-lg font-bold"> {rating ? rating : 0}</p>
              </span>
            </small>

            {/* Comment */}
            <div className="title flex md:inline-flex ">
              <textarea
                type="textarea"
                value={comment}
                onChange={(e) => setComment(e.target.value)}
                autoComplete="off"
                placeholder="add your comment here"
                className="input input-bordered input-lg w-full ml-5 border-gray-500"
              />
            </div>
            <div className="">
              <button className="btn btn-sm bg-gray-300 mt-4">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}
