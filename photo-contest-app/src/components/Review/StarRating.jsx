import { GiFallingStar } from 'react-icons/gi'
export default function StarRating({ score }) {
  const stars = Array(Math.floor(score) || 0).fill(0)

  return (
    <div>
      <div className="flex flex-row">
        {stars.map((_, index) => (
          <GiFallingStar
            className="cursor-pointer transition-shadow"
            key={index}
            size={18}
            color={'#E7D27C'}
          />
        ))}
      </div>
    </div>
  )
}
