import './SimpleJunkieParticipation.css'

import ParticipationInfo from '../ParticipationInfo/ParticipationInfo'

export const SimpleJunkieParticipation = ({ photo, author, title, story, addedOn, avatar }) => {
  return (
    <div className="flex flex-col w-full lg:flex-col">
      <div className="grid flex-grow h-full w-full bg-inherit rounded-box place-items-center">
        <ParticipationInfo
          author={author}
          title={title}
          description={story}
          createdOn={new Date(addedOn).toLocaleString('en-GB')}
          avatar={avatar}
        />
      </div>
      <div className="grid flex-grow h-full w-full bg-inherit place-items-center">
        <img className="h-[80%] w-[80%]" src={photo} alt="participationPhoto" />
      </div>      
      
    </div>
  )
}
