import { Link } from 'react-router-dom'
import { Transition } from '@headlessui/react'
export default function Transit({ userData, isOpen, setIsOpen, reference }) {
  return (
    <Transition
      show={isOpen}
      enter="transition ease-out duration-100 transform"
      enterFrom="opacity-0 scale-95"
      enterTo="opacity-100 scale-100"
      leave="transition ease-in duration-75 transform"
      leaveFrom="opacity-100 scale-100"
      leaveTo="opacity-0 scale-95">
      {
        <div className="lg:hidden" id="mobile-menu">
          <div ref={reference} className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
            <span
              onClick={() => setIsOpen(!isOpen)}
              className="hover:bg-slate-100 hover:text-accent block px-3 py-2 rounded-lg text-secondary font-medium">
              {userData !== null && <Link to={'/dashboard'}>Dashboard</Link>}
            </span>

            {userData?.role === 2 && (
              <span
                onClick={() => setIsOpen(!isOpen)}
                className="hover:bg-slate-100  hover:text-accent block px-3 py-2 rounded-lg text-secondary font-medium">
                <Link to={'createcontest'}>Create Contest</Link>
              </span>
            )}
            <span
              onClick={() => setIsOpen(!isOpen)}
              className="hover:bg-slate-100 hover:text-accent block px-3 py-2 rounded-lg text-secondary font-medium">
              <Link to={'inspirations'}>Inspirations</Link>
            </span>
          </div>
        </div>
      }
    </Transition>
  )
}
