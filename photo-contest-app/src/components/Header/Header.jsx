import './Header.css'

import { Link, useNavigate } from 'react-router-dom'
import React, { useContext, useRef, useState } from 'react'

import { AppContext } from '../../context/app.context'
import Transit from './Transit'
import defaultAvatar from '../../assets/empty.jpg'
import { logoutUser } from '../../services/auth.service'

export default function Header() {
  const [isOpen, setIsOpen] = useState(false)
  const navigate = useNavigate()
  const reference = useRef()
  const { addToast, setAppState, userData, ...appState } = useContext(AppContext)

  const logout = async () => {
    await logoutUser()
    setAppState({
      ...appState,
      user: null,
      userData: null,
    })

    navigate('/')
  }
  return (
    <div>
      <nav className="bg-primary mt-10">
        <div className="max-w-full px-4 sm:px-6 lg:px-8 divide-y divide-dashed ">
          <div className="divide-y divide-dashed divide-gray-50 pt-2 w-full"> </div>
          <div className="flex items-center justify-between h-16">
            <div className="flex items-center">
              <div
                className="text-2xl font-bold text-base-100 cursor-pointer"
                onClick={() => navigate('/')}>
                <span className="flex flex-wrap flex-col">
                  <div>ASA</div>
                </span>
              </div>
              <div className="hidden lg:block">
                <div className="ml-10 flex items-baseline space-x-4">
                  {userData !== null && (
                    <Link
                      to={'/dashboard'}
                      className=" hover:border text-base-100 px-3 py-2 rounded-lg text-lg font-medium">
                      Dashboard
                    </Link>
                  )}

                  {userData?.role === 2 && (
                    <span className="text-base-100 hover:border px-3 py-2 rounded-lg text-lg font-medium">
                      <Link to={'createcontest'}>Create Contests</Link>
                    </span>
                  )}
                  {userData?.user !== null && (
                    <span className="text-base-100 hover:border px-3 py-2 rounded-lg text-lg font-medium">
                      <Link to={'inspirations'}>Inspirations</Link>
                    </span>
                  )}
                </div>
              </div>
            </div>
            <>
              {appState?.user === null && (
                <ul className="menu menu-horizontal">
                  <span className="text-base-100 hover:border font-bold rounded-lg">
                    <Link to={'/login'}>Login</Link>
                  </span>
                </ul>
              )}

              {appState.user !== null && (
                <div className="flex flex-row">
                  <div className="dropdown dropdown-end text-lg text-base-100 font-bold">
                    <span className="flex items-center text-base-100 ">
                      {`Hello, ${userData?.username}! ${' '}`}
                      <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                        <div className="w-10 rounded-full">
                          <img src={userData?.avatarUrl ? userData?.avatarUrl : defaultAvatar} />
                        </div>
                      </label>
                    </span>
                    <ul
                      tabIndex={0}
                      className="menu menu-compact dropdown-content mt-3 p-2 shadow text-accent bg-base-100 rounded-box w-52">
                      <li>
                        <Link to={`/profile/${userData?.username}`} className="justify-between">
                          Profile
                        </Link>
                      </li>
                      <li>
                        <span onClick={logout}>Logout</span>
                      </li>
                    </ul>
                  </div>
                </div>
              )}
            </>
            {userData !== null && (
              <div className="-mr-2 flex lg:hidden">
                <button
                  onClick={() => setIsOpen(!isOpen)}
                  type="button"
                  className="border inline-flex items-center justify-center p-2 rounded-lg text-base-100 hover:border  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-slate-300  focus:ring-slate-50"
                  aria-controls="mobile-menu"
                  aria-expanded="false">
                  <span className="sr-only">Open main menu</span>
                  {!isOpen ? (
                    <svg
                      className="block h-6 w-6"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      aria-hidden="true">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M4 6h16M4 12h16M4 18h16"
                      />
                    </svg>
                  ) : (
                    <svg
                      className="block h-6 w-6"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      aria-hidden="true">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                  )}
                </button>
              </div>
            )}
          </div>
          <hr className="divide-y divide-dashed divide-gray-50 pb-2" />
        </div>
        <Transit userData={userData} isOpen={isOpen} setIsOpen={setIsOpen} reference={reference} />
      </nav>
    </div>
  )
}
