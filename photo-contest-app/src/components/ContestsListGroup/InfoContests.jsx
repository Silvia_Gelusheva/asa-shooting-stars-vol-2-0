export default function InfoContests({ phase }) {
  return (
    <div className="flex flex-col w-full lg:flex-row my-8">
      <div className="card-container h-32 bg-inherit place-items-center font-bold text-4xl uppercase">
        {phase === 'registration' && <p> Ongoing contests</p>}
        {phase === 'review' && <p> Assessment</p>}
        {phase === 'finished' && <p> Previous contests</p>}
      </div>
      <div className="divider lg:divider-horizontal"></div>
      {phase === 'registration' && (
        <div className="h-32 card-container bg-inherit rounded-box place-items-center px-8">
          How it works?
          <br />
          This contest is expert judged only with no rating by the crowd. You will be able to find
          out how you did when the judging or rating period has closed.
        </div>
      )}
      {phase === 'review' && (
        <div className="h-32 card-container bg-inherit rounded-box place-items-center px-8">
          How it works?
          <br />
          Every photo submitted will be available for the jury to rate once the submissions period
          has ended. You can see all the images uploaded to a contest, but will need to rate them to
          see how they’re ranked once the rating period begins.
        </div>
      )}
      {phase === 'finished' && (
        <div className="h-32 card-container bg-inherit rounded-box place-items-center px-8">
          Congratulations to the winners of ASA Shooting Stars Contests!
        </div>
      )}
    </div>
  )
}
