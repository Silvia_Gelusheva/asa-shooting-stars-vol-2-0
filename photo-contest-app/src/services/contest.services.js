import {
  get,
  push,
  ref,
  update,
} from 'firebase/database'

import { db } from '../firebase/config'

export const addContestParticipation = async (
  title,
  story,
  url,
  username,
  contestId
  // participationId
) => {
  // const participationId = v4()

  const participation = {
    title,
    story,
    photo: url,
    contestId,
    author: username,
    score: 0,
    scores: {},
    // comments: {},
    addedOn: Date.now(),
  }

  const { key } = await push(ref(db, 'participations'), participation)

  return update(ref(db), {
    [`users/${username}/participations/${key}`]: true,
    [`users/${username}/contestIds/${contestId}`]: true,
    [`contests/${contestId}/participations/${username}`]: key,
  })
}

export const createContest = async (
  createdOn,
  title,
  category,
  url,
  phaseOneEnd,
  phaseTwoEnd,
  juryList,
  invitational,
  enrolledJunkies = {}
) => {
  const contest = {
    createdOn: createdOn,
    title,
    category,
    cover: url,
    phaseOneEndTime: phaseOneEnd,
    phaseTwoEndTime: phaseTwoEnd,
    juryList,
    invitational,
    enrolledJunkies
  }

  if (Object.keys(enrolledJunkies).length !== 0) contest.enrolled = enrolledJunkies

  const { key } = await push(ref(db, `contests/`), contest)
  return update(ref(db), { [`contests/${key}/contestId`]: key })
}

export const getParticipationById = async (id) => {
  const snapshot = await get(ref(db, `participations/${id}`))
  if (!snapshot.exists()) throw new Error(`Participation with id: ${id} does not exist!`)

  return {
    ...snapshot.val(),
    id,
  }
}

export const getParticipationByAuthor = async (query) => {
  const snapshot = await get(ref(db, 'participations'))

  if (!snapshot.exists()) throw new Error(`Something went wrong!`)

  return Object.keys(snapshot.val())
    .map((key) => ({ ...snapshot.val()[key], id: key }))
    .filter((participation) => participation.author.toLowerCase().includes(query.toLowerCase()))
}

export const getAllContests = async () => {
  const snapshot = await get(ref(db, 'contests'))

  return Object.values(snapshot.val())
}

export const getContestById = async (id) => {
  const snapshot = await get(ref(db, `contests/${id}`))
  if (!snapshot.exists()) throw new Error(`Contest with id: ${id} does not exist!`)
  const result = { ...snapshot.val() }
  result.enrolled === undefined
    ? (result.enrolled = [])
    : (result.enrolled = Object.keys(result.enrolled))

  result.participants === undefined ? (result.participants = {}) : result.participants
  return result
}

// export const updateCoverPhoto = (contestId, url) => {
//   return update(ref(db), {
//     [`contests/${contestId}/cover`]: url,
//   })
// }

export const enrollParticipant = async (contestId, username) => {
  return await update(ref(db), { [`contests/${contestId}/enrolled/${username}`]: true })
}

export const deleteContest = async (contestId, username) => {
  return await update(ref(db), {
    [`contests/${contestId}`]: null,
    [`participations/participationId/${contestId}`]: null,
    [`users/${username}/contestsIds/${contestId}`]: null,
  })
}