import { async } from '@firebase/util'
import {
  ref,
  push,
  get,
  set,
  update,
  query,
  equalTo,
  orderByChild,
  orderByKey,
  onValue,
} from 'firebase/database'

import { db } from '../firebase/config'

export const numberOfJuryMembers = async (contestId) => {
  const snapshot = await get(ref(db, `contests/${participationId}/juryList`))
  if (!snapshot.exists()) return 0

  return Object.keys(snapshot.val()).length
}

export const updateFinalParticipationScore = async (score, participationId) => {
  return update(ref(db), { [`participations/${participationId}/score`]: score })
}
export const updateRankingPointsDB = async (points, username) => {
  return update(ref(db), { [`users/${username}/points`]: points })
}
export const getRankingPointsDB = async (username) => {
  const snapshot = await get(ref(db, `users/${username}/points`))

  return snapshot.val()
}

export const updateContestEvaluation = async (contestId) => {
  return update(ref(db), { [`contests/${contestId}/evaluated`]: 'true' })
}
