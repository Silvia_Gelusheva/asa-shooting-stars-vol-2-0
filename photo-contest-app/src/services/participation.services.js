import { ref, push, get, set, update, query, equalTo,  orderByChild,  orderByKey} from 'firebase/database'
import { db } from '../firebase/config'
export const getParticipationById = async (id) => {
    const snapshot = await get(ref(db, `participations/${id}`))

    if (!snapshot.exists()) throw new Error(`Participation with id ${id} does not exist!`)

    return { ...snapshot.val(), id }
}

export const getAllParticipationsByContestId = async (contestId) => {
    const snapshot = await get(ref(db, `contests/${contestId}/participations`))
    if (!snapshot.exists()) throw new Error(`Participation with id ${contestId} does not exist!`)
    const result = Object.values(snapshot.val())

    return result
}

