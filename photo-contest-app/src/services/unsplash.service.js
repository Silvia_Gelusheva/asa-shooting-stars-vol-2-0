import { UNSPLASH_SEARCH_PHOTOS_APP_KEY } from '../common/constants.js'


export const fetchPhotosFromUnsplash = async (query) => {
const response = await fetch(`https://api.unsplash.com/search/photos?query=${query}&client_id=${UNSPLASH_SEARCH_PHOTOS_APP_KEY}`)
//  console.log('fetch');
 return response.json()
}

