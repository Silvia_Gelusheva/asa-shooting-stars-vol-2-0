/* eslint-disable no-unsafe-optional-chaining */
import {
  equalTo,
  get,
  orderByChild,
  orderByKey,
  push,
  query,
  ref,
  set,
  update,
} from 'firebase/database'

import { db } from '../firebase/config'
import { photoJunkieRanking } from '../common/enums/photojunkie-ranking.enum'
import { userRole } from '../common/enums/user-role.enum'

export const getUser = async (username) => {
  const snapshot = await get(ref(db, `users/${username}`))

  return snapshot.val()
}

export const getUserByPhone = async (phone) => {
  const snapshot = await get(query(ref(db, 'users'), orderByChild('phone'), equalTo(phone)))

  return snapshot.val()
}

export const createUser = async (
  uid,
  firstname,
  lastname,
  username, 
  isActive = true,
  points = 0,
  role = userRole.PHOTO_JUNKIE,
  rank = photoJunkieRanking.JUNKIE
) => {
  const user = await getUser(username)

  if (user !== null) throw new Error(`Username ${username} already exists!`)

  // const userPhone = await getUserByPhone(phone)

  // if (userPhone !== null) throw new Error(`Phone number ${phone} has already been registered!`)

  const userData = {
    uid,
    firstname,
    lastname,
    username,   
    isActive,
    points,
    role,
    rank,
    registeredOn: Date.now(),
  }

  await set(ref(db, `users/${username}`), userData)

  return { ...userData }
}

export const getUserById = async (uid) => {
  const snapshot = await get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)))
  const { reviewIds, photoIds, points, ...value } =
    snapshot.val()?.[Object.keys(snapshot.val())?.[0]]

  return {
    ...value,
    // reviewIds: reviewIds ? Object.keys(reviewIds) : [],
    // photoIds: photoIds ? Object.keys(photoIds) : [],
    // points: points ? points : 0,
  }
}

export const updateUserProfilePic = (username, url) => {
  return update(ref(db), {
    [`users/${username}/avatarUrl`]: url,
  })
}

export const getAllUsers = async () => {
  const snapshot = await get(ref(db, 'users'))

  if (!snapshot.exists()) {
    return []
  }
  return Object.keys(snapshot.val()).map((key) => ({ ...snapshot.val()[key] }))
}

export const getAvatarByUsername = async (username) => {
  const snapshot = await get(ref(db, `users/${username}/avatarUrl`))

  if (!snapshot.exists()) {
    return ''
  }
  return snapshot.val()
}
