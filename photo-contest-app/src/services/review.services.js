import { error } from 'daisyui/src/colors'
import {
  ref,
  push,
  get,
  set,
  update,
  query,
  equalTo,
  orderByChild,
  orderByKey,
} from 'firebase/database'
import { db } from '../firebase/config'

export const addReview = async (jury, participationId, rating, content, username) => {
  const review = {
    jury,
    participationId,
    rating,
    content,
    createdOn: Date.now(),
  }
  // const { key } = await push(ref(db, `participations/${participationId}/reviews`), review)


  // TODO username = participation.author
  return update(ref(db), {
    [`participations/${participationId}/reviews/${jury}`]: review,
  })
}

export const getAllParticipationReviewsById = async (participationId) => {
  const snapshot = await get(ref(db, `participations/${participationId}/reviews`))
  // console.log(Object.keys(snapshot.val()))
  if (!snapshot.exists()) return []
  // throw new Error(`Reviews with id ${participationId} does not exist!`)

  return Object.values(snapshot.val())
}
export const getReviewById = async (id) => {
  const snapshot = await get(ref(db, `reviews/${id}`))
  console.log(snapshot.val())
  if (!snapshot.exists()) throw new Error(`Review with id ${id} does not exist!`)

  return {
    ...snapshot.val(),
    id,
  }
}
