<!-- <img align="left" src='./public/ASA SS.png' alt="logo" width='70px'>    <p>&nbsp;</p> -->


#  **ASA *Shooting Stars***


### Description

Upgraded version of our final project assignment for Alpha JavaScript 42 at Telerik Academy.

A Single Page Application for creating online photo contests, aiming to inspire others and a place to share beauty of the world.

Each photo contest has three parts:
- ***open*** - users can enroll in a contest and upload a photo;
- ***review*** - time for jury to rate every participation; 
- ***finished*** - photos and winners can be seen by all users. 

The application has two main parts.
For organizers, where, the they can organize photo contests and rate them and for photo junkies – everyone who likes photography can register and participate in different contests either after invitation or enrolling themselves. Junkies with certain ranking can be invited to be a part of the jury.
<br>

### Project information

- JavaScript
- React JS
- CSS
- Daisy UI
- Tailwind UI
- Tailwind CSS
- Firebase
- ESLint
<br>

### Setup

Open the root directory photo-contest-app folder. Install all packages by running:
`npm install`

Then run:

`npm run dev`

<br>

You can test the application and see for yourself what every user can do by login to the application as a Junkie or Organiser, using the following credentials:

*email:* junkie_test@test.js
*password:* 123456

*email:* organiser_test@test.js
*password:* 123456

You can register a new user if you prefer.
<br>

#### **Landing page**

<img src='./src/assets/LandingPage.png' width='800px'>

<br/>

#### **Register**

<img src='./src/assets/Register.png' width='800px'> 

<br/>

#### **Login**

<img src='./src/assets/Login.png' width='800px'> 

<br/>

#### **Dashboard User**

<img src='./src/assets/Dashboard-user.png' width='800px'> 

<br/>

#### **Dashboard Admin**

<img src='./src/assets/Dashboard-admin.png' width='800px'> 

<br/>

#### **Enroll**

<img src='./src/assets/Enroll.png' width='800px'> 

<br/>

#### **Photo Upload**

<img src='./src/assets/ParticipationForm.png' width='800px'> 

<br/>

#### **Finished contests**

<img src='./src/assets/FinishedContests.png' width='800px'>
<br/> 
<img src='./src/assets/FinishedContests1.png' width='800px'> 

<br/>

#### **Finished single contest**

<img src='./src/assets/FinishedContests2.png' width='800px'> 

<br/>

#### **Reviews**

<img src='./src/assets/ReviewContests.png' width='800px'>
<br/> 
<img src='./src/assets/ReviewContests1.png' width='800px'> 

<br/>

#### **Create Contest**

<img src='./src/assets/CreateContest.png' width='800px'> 

<br/>

<!-- #### **Create Contest Invitational**



<br/> -->

#### **Profile**

<img src='./src/assets/Profile.png' width='800px'> 

<br/>

#### **My Shots**

<img src='./src/assets/MyShots.png' width='800px'> 

<br/>

#### **Inspiration**

<img src='./src/assets/Inspirations.png' width='800px'> 

<br/>

