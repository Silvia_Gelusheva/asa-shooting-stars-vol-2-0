/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: '#141826', 
          secondary: '#C0C0C0',
          // accent: '#e56b6f',
          accent: '#c79851',
          neutral: '#F8F8F8',
          'base-100': '#f9f9f8',
          info: '#ffd166', 
          success: '#ffb8ed',
          warning: '#ff9234',
          error: '#b91c1c',
        },
      },
    ],
  },
  theme: {
    extend: {
      backgroundImage: {
        'camera': "url('https://wallpaperaccess.com/full/375785.jpg')",     
      },
    },
  },
  plugins: [require('daisyui'), require('tailwind-scrollbar-hide')],
}
